package alarmwork.example.job.task.alarmwork_api;

import alarmwork.example.job.task.alarmwork.AlarmExerciseManager;
import alarmwork.example.job.task.alarmwork.AppAlarmManager;
import android.content.Context;
import square.green.com.utils.api.alarmrepo.AlarmRepoApi;
import task.job.example.utils.api.AlarmWorkApi;

public class AlarmWorkFactory implements AlarmWorkLibApi {

  public AlarmWorkFactory(Context context, AlarmRepoApi repoAlarmDao) {
    AppAlarmManager.setupEnvironment(context, repoAlarmDao, AlarmExerciseManager.getInstance());
  }


  @Override
  public AlarmWorkApi provideAppAlarmManager() {
    return AppAlarmManager.getInstance(null, null, AlarmExerciseManager.getInstance());
  }


}
