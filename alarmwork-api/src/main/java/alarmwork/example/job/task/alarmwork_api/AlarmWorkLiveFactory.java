package alarmwork.example.job.task.alarmwork_api;

import alarmwork.example.job.task.alarmwork.AlarmExerciseManager;
import task.job.example.utils.api.AlarmWorkLiveApi;

public class AlarmWorkLiveFactory implements AlarmWorkLiveLibApi {

    @Override
    public AlarmWorkLiveApi provideAlarmWorkLiveDataApi() {
        return AlarmExerciseManager.getInstance();
    }
}
