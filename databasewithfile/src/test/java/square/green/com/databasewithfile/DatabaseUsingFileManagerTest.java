package square.green.com.databasewithfile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import task.job.example.utils.model.Alarm;
import task.job.example.utils.model.AlarmBuilder;

public class DatabaseUsingFileManagerTest {

  String databasePath = "";
  private DatabaseUsingFileManager databaseUsingFileManager = DatabaseUsingFileManager
      .getInstance(databasePath, true);

  @Before
  public void setUp() {
    assertTrue(databaseUsingFileManager.createDatabaseFile());
  }

  @After
  public void tearDown() {
    assertTrue(databaseUsingFileManager.deleteFileDatabase());
  }


  @Test
  public void readEmptyFile() {

    JSONObject result = databaseUsingFileManager.getJsonFromDatabaseFile();
    assertEquals(result.length(), 0);
  }

  @Test
  public void getInstanceCreateFile() {
    assertTrue(databaseUsingFileManager.databaseFileIsExist());
  }

  @Test
  public void writeAlarmOneAlarm() {

    Alarm alarm = AlarmBuilder.generateDefaultAlarmBuilder(101).build();

    assertTrue(databaseUsingFileManager.writeAlarm(alarm));

    List<Alarm> alarms = databaseUsingFileManager.getAlarms();

    assertEquals(alarms.size(), 1);

    Alarm alarmFromDatabase = alarms.get(0);
    assertEquals(alarmFromDatabase.getId(), 101);
    assertEquals(alarmFromDatabase.getRingtone(), "ololo");
    assertEquals(alarmFromDatabase.getDays(), 22);
    assertEquals(alarmFromDatabase.getHour(), 21);
    assertEquals(alarmFromDatabase.getMinute(), 47);
    assertEquals(alarmFromDatabase.getAnswer(), "qwe");
    assertEquals(alarmFromDatabase.getAnswerType(), 2);
    assertTrue(alarmFromDatabase.isVibrate());
  }

  @Test
  public void writeAlarmTwoDifferentAlarm() {

    Alarm alarm = AlarmBuilder.generateDefaultAlarmBuilder(101).build();

    assertTrue(databaseUsingFileManager.writeAlarm(alarm));

    alarm = AlarmBuilder.generateDefaultAlarmBuilder(102).build();

    assertTrue(databaseUsingFileManager.writeAlarm(alarm));

    List<Alarm> alarms = databaseUsingFileManager.getAlarms();

    assertEquals(alarms.size(), 2);
  }

  @Test
  public void writeAlarmTwoTheSameAlarm() {

    Alarm alarm = AlarmBuilder.generateDefaultAlarmBuilder(101).build();

    assertTrue(databaseUsingFileManager.writeAlarm(alarm));

    alarm = AlarmBuilder.generateDefaultAlarmBuilder(101).withRingtone("trololo").build();

    assertTrue(databaseUsingFileManager.writeAlarm(alarm));

    List<Alarm> alarms = databaseUsingFileManager.getAlarms();

    assertEquals(alarms.size(), 1);

    Alarm alarmFromDatabase = alarms.get(0);
    assertEquals(alarmFromDatabase.getId(), 101);
    assertEquals(alarmFromDatabase.getRingtone(), "trololo");
    assertEquals(alarmFromDatabase.getDays(), 22);
    assertEquals(alarmFromDatabase.getHour(), 21);
    assertEquals(alarmFromDatabase.getMinute(), 47);
    assertEquals(alarmFromDatabase.getAnswer(), "qwe");
    assertEquals(alarmFromDatabase.getAnswerType(), 2);
    assertTrue(alarmFromDatabase.isVibrate());
  }

  @Test
  public void deleteAlarmFromEmptyFile() {
    assertEquals(databaseUsingFileManager.deleteAlarm(101), -1);
  }

  @Test
  public void deleteAlarmFromFileWithOneNote() {

    Alarm alarm = AlarmBuilder.generateDefaultAlarmBuilder(101).build();

    assertTrue(databaseUsingFileManager.writeAlarm(alarm));

    alarm = AlarmBuilder.generateDefaultAlarmBuilder(102).build();

    assertTrue(databaseUsingFileManager.writeAlarm(alarm));

    assertEquals(databaseUsingFileManager.deleteAlarm(102), 1);

    assertEquals(databaseUsingFileManager.deleteAlarm(102), -1);

    assertEquals(databaseUsingFileManager.deleteAlarm(101), 0);

    assertEquals(databaseUsingFileManager.deleteAlarm(100), -1);
  }

  @Test
  public void getNullWhenWeWantGetAlarmWhichIsNotAdded() {
    assertNull(databaseUsingFileManager.getAlarmById(101));
  }

  @Test
  public void getAlarmWhenWeAddedAlarmById() {

    Alarm alarm = AlarmBuilder.generateDefaultAlarmBuilder(101).build();

    assertTrue(databaseUsingFileManager.writeAlarm(alarm));

    Alarm alarmFromDatabase = databaseUsingFileManager.getAlarmById(101);

    assertNotNull(alarmFromDatabase);

    assertEquals(alarmFromDatabase.getId(), 101);
    assertEquals(alarmFromDatabase.getRingtone(), "ololo");
    assertEquals(alarmFromDatabase.getDays(), 22);
    assertEquals(alarmFromDatabase.getHour(), 21);
    assertEquals(alarmFromDatabase.getMinute(), 47);
    assertEquals(alarmFromDatabase.getAnswer(), "qwe");
    assertEquals(alarmFromDatabase.getAnswerType(), 2);
    assertTrue(alarmFromDatabase.isVibrate());

  }

}