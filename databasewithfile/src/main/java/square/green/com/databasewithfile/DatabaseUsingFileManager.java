package square.green.com.databasewithfile;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import square.green.com.utils.api.databasealarm.DatabaseAlarmApi;
import task.job.example.utils.model.Alarm;

public class DatabaseUsingFileManager implements DatabaseAlarmApi {

  private File databaseFile;
  private static volatile DatabaseUsingFileManager Instance;

  public static DatabaseUsingFileManager getInstance(String path,
      boolean isCreateFileInGetInstance) {
    DatabaseUsingFileManager localInstance = Instance;
    if (localInstance == null) {
      synchronized (DatabaseUsingFileManager.class) {
        localInstance = Instance;
        if (localInstance == null) {
          Instance = localInstance = new DatabaseUsingFileManager(path, isCreateFileInGetInstance);
        }
      }
    }
    return localInstance;
  }


  private DatabaseUsingFileManager(String path, boolean isCreateFile) {
    String nameDatabase = "alarmDatabase.txt";
    databaseFile = new File(path, nameDatabase);
    if (isCreateFile) {
      createDatabaseFile();
    }
  }

  boolean createDatabaseFile() {
    boolean result = false;
    try {
      result = databaseFile.createNewFile();
      if (result) {
        FileWriter fileWriter = new FileWriter(databaseFile);
        fileWriter.write("{}");
        fileWriter.close();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return result;
  }

  boolean databaseFileIsExist() {
    return databaseFile.exists();

  }

  JSONObject getJsonFromDatabaseFile() {

    JSONObject jsonObject = new JSONObject();
    try {
      FileInputStream fileInputStream = new FileInputStream(databaseFile);
      byte[] data = new byte[(int) databaseFile.length()];
      fileInputStream.read(data);
      fileInputStream.close();

      jsonObject = new JSONObject(new String(data));

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (JSONException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return jsonObject;
  }

  @Override
  @Nullable
  public Alarm getAlarmById(long alarmId) {

    JSONObject jsonObject = getJsonFromDatabaseFile();

    JSONArray alarms = getAlarmJSONArray(jsonObject);
    List<Alarm> massAlarms = parseJsonArrayToAlarmList(alarms);
    for (Alarm massAlarm : massAlarms) {
      if (massAlarm.getId() == alarmId) {
        return massAlarm;
      }
    }

    return null;
  }

  private void writeJsonObjectsToFile(JSONObject jsonObject) {

    try {
      FileWriter fileWriter = new FileWriter(databaseFile);
      fileWriter.write(jsonObject.toString());
      fileWriter.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  @Override
  public boolean writeAlarm(@NonNull Alarm alarm) {

    boolean writeResult = false;

    JSONObject alarmJson = AlarmJsonParser.createAlarmJsonNote(alarm);
    JSONObject jsonObject = getJsonFromDatabaseFile();

    try {
      JSONArray alarms = getAlarmJSONArray(jsonObject);
      deleteAlarm(alarms, alarm.getId());

      alarms.put(alarmJson);
      jsonObject.put("Alarm", alarms);

      writeJsonObjectsToFile(jsonObject);
      writeResult = true;
    } catch (JSONException e) {
      e.printStackTrace();
    }

    return writeResult;
  }

  @Override
  public int deleteAlarm(long alarmId) {

    JSONObject jsonObject = getJsonFromDatabaseFile();
    JSONArray jsonArray = getAlarmJSONArray(jsonObject);
    int position = deleteAlarm(jsonArray, alarmId);
    if (position == -1) {
      return position;
    }

    try {
      jsonObject.put("Alarm", jsonArray);
    } catch (JSONException e) {
      e.printStackTrace();
    }

    writeJsonObjectsToFile(jsonObject);

    return position;

  }

  private JSONArray getAlarmJSONArray(JSONObject jsonObject) {
    JSONArray alarms = jsonObject.optJSONArray("Alarm");
    if (alarms == null) {
      alarms = new JSONArray();
    }
    return alarms;
  }

  private int deleteAlarm(JSONArray alarms, long alarmId) {

    try {
      for (int i = 0; i < alarms.length(); i++) {
        ParsingResult parsingResult = AlarmJsonParser
            .parseJsonStringToAlarm(alarms.getJSONObject(i));
        if (parsingResult.isSuccess && ((Alarm) (parsingResult.value)).getId() == alarmId) {
          alarms.remove(i);
          return i;
        }
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return -1;
  }

  @Override
  @NonNull
  public List<Alarm> getAlarms() {

    JSONObject jsonFromDatabase = getJsonFromDatabaseFile();
    JSONArray alarms = jsonFromDatabase.optJSONArray("Alarm");
    if (alarms == null) {
      return Collections.emptyList();
    }

    return parseJsonArrayToAlarmList(alarms);
  }

  @NonNull
  private List<Alarm> parseJsonArrayToAlarmList(@NonNull JSONArray alarms) {
    List<Alarm> result = new ArrayList<>();

    try {
      for (int i = 0; i < alarms.length(); i++) {
        ParsingResult parseResult = AlarmJsonParser.parseJsonStringToAlarm(alarms.getJSONObject(i));
        if (parseResult.isSuccess) {
          result.add((Alarm) parseResult.value);
        }
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return result;
  }

  boolean deleteFileDatabase() {
    return databaseFile.delete();
  }

  static class ParsingResult {

    Object value = new Object();
    boolean isSuccess = false;
  }

}
