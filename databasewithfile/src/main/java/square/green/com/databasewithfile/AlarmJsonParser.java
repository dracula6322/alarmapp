package square.green.com.databasewithfile;

import androidx.annotation.NonNull;
import org.json.JSONException;
import org.json.JSONObject;
import square.green.com.databasewithfile.DatabaseUsingFileManager.ParsingResult;
import task.job.example.utils.model.Alarm;
import task.job.example.utils.model.AlarmBuilder;

class AlarmJsonParser {

  @NonNull
  static ParsingResult parseJsonStringToAlarm(@NonNull JSONObject alarmJsonObject) {

    ParsingResult parsingResult = new ParsingResult();

    try {
      Alarm alarm = AlarmBuilder.anAlarm()
          .withId(alarmJsonObject.getLong("id"))
          .withAnswer(alarmJsonObject.getString("answer"))
          .withDays(alarmJsonObject.getInt("days"))
          .withMinute(alarmJsonObject.getInt("minute"))
          .withHour(alarmJsonObject.getInt("hour"))
          .withAnswer(alarmJsonObject.getString("answer"))
          .withAnswerType(alarmJsonObject.getInt("answerType"))
          .withRingtone(alarmJsonObject.getString("ringtone"))
          .withIsVibrate(alarmJsonObject.getBoolean("isVibrate"))
          .build();
      parsingResult.isSuccess = true;
      parsingResult.value = alarm;
    } catch (JSONException e) {
      e.printStackTrace();
    }

    return parsingResult;
  }

  @NonNull
  static JSONObject createAlarmJsonNote(@NonNull Alarm alarm) {

    JSONObject jsonObject = new JSONObject();
    try {
      jsonObject.put("id", alarm.getId());
      jsonObject.put("ringtone", alarm.getRingtone());
      jsonObject.put("days", alarm.getDays());
      jsonObject.put("hour", alarm.getHour());
      jsonObject.put("minute", alarm.getMinute());
      jsonObject.put("answer", alarm.getAnswer());
      jsonObject.put("answerType", alarm.getAnswerType());
      jsonObject.put("isVibrate", alarm.isVibrate());

    } catch (JSONException e) {
      e.printStackTrace();
    }

    return jsonObject;
  }

}
