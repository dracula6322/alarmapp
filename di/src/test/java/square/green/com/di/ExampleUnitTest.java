package square.green.com.di;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.junit.Test;
import task.job.example.utils.di.ApplicationComponent;
import task.job.example.utils.di.ApplicationComponent.Builder;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

  @Test
  public void addition_isCorrect() {
    assertEquals(4, 2 + 2);
  }

  @Test
  public void syntheticTest() {

    Constructor<?>[] constructions = ApplicationComponent.class.getConstructors();
    for (Constructor<?> construction : constructions) {
      assertFalse(construction.isSynthetic());
    }

    constructions = ApplicationComponent.class.getDeclaredConstructors();
    for (Constructor<?> construction : constructions) {
      assertFalse(construction.isSynthetic());
    }

    constructions = ApplicationComponent.class.getDeclaredConstructors();
    for (Constructor<?> construction : constructions) {
      assertFalse(construction.isSynthetic());
    }

    Method[] methods = ApplicationComponent.class.getDeclaredMethods();
    for (Method method : methods) {
      assertFalse(method.isSynthetic());
    }

    methods = ApplicationComponent.class.getMethods();
    for (Method method : methods) {
      assertFalse(method.isSynthetic());
    }

    constructions = Builder.class.getConstructors();
    for (Constructor<?> construction : constructions) {
      assertFalse(construction.isSynthetic());
    }

    constructions = Builder.class.getDeclaredConstructors();
    for (Constructor<?> construction : constructions) {
      assertFalse(construction.isSynthetic());
    }

    constructions = Builder.class.getDeclaredConstructors();
    for (Constructor<?> construction : constructions) {
      assertFalse(construction.isSynthetic());
    }

    methods = ApplicationComponent.Builder.class.getDeclaredMethods();
    for (Method method : methods) {
      assertFalse(method.isSynthetic());
    }

    methods = ApplicationComponent.Builder.class.getMethods();
    for (Method method : methods) {
      assertFalse(method.isSynthetic());
    }
  }

}