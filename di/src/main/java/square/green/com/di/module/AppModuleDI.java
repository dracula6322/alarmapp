package square.green.com.di.module;

import android.content.Context;
import square.green.com.di.utils.Module;

public class AppModuleDI implements Module<Context> {

  private final Context context;

    public AppModuleDI(Context context) {
        this.context = context;
    }

    @Override
    public Context get() {
        return context;
    }
}
