package square.green.com.di;

public final class ApplicationComponentImpl {

  private static volatile ApplicationComponentImpl Instance;
  private ApplicationComponent applicationComponent;

  public static ApplicationComponentImpl getInstance() {
    ApplicationComponentImpl localInstance = Instance;
    if (localInstance == null) {
      synchronized (ApplicationComponentImpl.class) {
        localInstance = Instance;
        if (localInstance == null) {
          Instance = localInstance = new ApplicationComponentImpl();
        }
      }
    }
    return localInstance;
  }

  public ApplicationComponent getApplicationComponent() {
    return applicationComponent;
  }

  public void setApplicationComponent(
      ApplicationComponent applicationComponent) {
    this.applicationComponent = applicationComponent;
  }
}
