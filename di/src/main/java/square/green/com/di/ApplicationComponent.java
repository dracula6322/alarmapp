package square.green.com.di;

import static square.green.com.di.utils.DoubleCheck.checkNotNull;

import android.content.Context;
import java.util.List;
import square.green.com.di.utils.DoubleCheck;
import square.green.com.di.utils.Lazy;
import square.green.com.di.utils.Module;
import square.green.com.di.utils.ModuleSetter;
import square.green.com.di.utils.ModuleSetter2;
import square.green.com.utils.Provider;
import square.green.com.utils.api.alarmrepo.AsyncAlarmRepoApi;
import square.green.com.utils.api.databasealarm.DatabaseAlarmApi;
import task.job.example.utils.api.AlarmWorkApi;
import task.job.example.utils.api.AlarmWorkLiveApi;
import task.job.example.utils.model.Alarm;

public final class ApplicationComponent {

  private Module<Context> contextModule;
  private Module<DatabaseAlarmApi> databaseModule;
  private Lazy<AsyncAlarmRepoApi> alarmRepoModule;
  private Module<AlarmWorkApi> alarmWorkerModule;
  private Module<AlarmWorkLiveApi> alarmWorkerLiveModule;

  @SuppressWarnings("WeakerAccess")
  public ApplicationComponent(Builder builder) {

    this.contextModule = DoubleCheck.provider(builder.contextModule);
    this.databaseModule = DoubleCheck.provider(builder.databaseModule);
    this.alarmWorkerModule = DoubleCheck.provider(builder.alarmWorkerModule);
    this.alarmRepoModule = DoubleCheck.lazy(builder.alarmRepoModule);
    this.alarmWorkerLiveModule = DoubleCheck.provider(builder.alarmWorkerLiveModule);
  }

  public AsyncAlarmRepoApi provideAlarmRepoApi() {
    return alarmRepoModule.get();
  }

  public AlarmWorkLiveApi provideAlarmWorkLiveApi() {
    return alarmWorkerLiveModule.get();
  }

  public AlarmWorkApi provideAlarmWorker() {
    return alarmWorkerModule.get();
  }

  public Context provideContext() {
    return contextModule.get();
  }

  public static final class Builder {

    Module<Context> contextModule;
    Module<DatabaseAlarmApi> databaseModule;
    Module<AsyncAlarmRepoApi> alarmRepoModule;
    Module<AlarmWorkApi> alarmWorkerModule;
    Module<AlarmWorkLiveApi> alarmWorkerLiveModule;

    public ApplicationComponent build() {
      if (contextModule == null) {
        throw new IllegalStateException(Context.class.getCanonicalName() + " must be set");
      }

      return new ApplicationComponent(this);
    }

    public Builder contextModule(Module<Context> contextModule) {
      this.contextModule = checkNotNull(contextModule);
      return this;
    }

    public <T extends Module<DatabaseAlarmApi> & ModuleSetter<Context>> Builder databaseModule(
        T alarmDatabaseApiModule) {
      alarmDatabaseApiModule.set(contextModule.get());
      this.databaseModule = checkNotNull(alarmDatabaseApiModule);

      return this;
    }

    public <T extends Module<AsyncAlarmRepoApi> & ModuleSetter<DatabaseAlarmApi> & ModuleSetter2<Provider<List<Alarm>>>> Builder alarmRepoModule(
        T alarmRepoApiModule) {
      alarmRepoApiModule.set(databaseModule.get());
      this.alarmRepoModule = checkNotNull(alarmRepoApiModule);
      return this;
    }

    public <T extends Module<AlarmWorkApi> & ModuleSetter<AsyncAlarmRepoApi> & ModuleSetter2<Context>> Builder alarmWorkerModule(
        T alarmWorkerModule) {
      alarmWorkerModule.set(alarmRepoModule.get());
      alarmWorkerModule.set2(contextModule.get());
      this.alarmWorkerModule = checkNotNull(alarmWorkerModule);
      return this;
    }

    public <T extends Module<AlarmWorkLiveApi>> Builder alarmWorkerLiveModule(
        T alarmWorkerLiveModule) {
      this.alarmWorkerLiveModule = checkNotNull(alarmWorkerLiveModule);
      return this;
    }
  }

}
