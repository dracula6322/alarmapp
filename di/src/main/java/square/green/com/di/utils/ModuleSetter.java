package square.green.com.di.utils;

public interface ModuleSetter<T> {

    void set(T value);

}
