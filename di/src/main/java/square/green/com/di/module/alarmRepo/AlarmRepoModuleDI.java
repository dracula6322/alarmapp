package square.green.com.di.module.alarmRepo;

import java.util.List;
import square.green.com.alarmrepo_api.AlarmRepoFactory;
import square.green.com.di.utils.Module;
import square.green.com.di.utils.ModuleSetter;
import square.green.com.di.utils.ModuleSetter2;
import square.green.com.utils.Provider;
import square.green.com.utils.api.alarmrepo.AsyncAlarmRepoApi;
import square.green.com.utils.api.databasealarm.DatabaseAlarmApi;
import task.job.example.utils.model.Alarm;

public class AlarmRepoModuleDI implements Module<AsyncAlarmRepoApi>, ModuleSetter<DatabaseAlarmApi>,
    ModuleSetter2<Provider<List<Alarm>>> {

  private AsyncAlarmRepoApi alarmRepoApi;
  private DatabaseAlarmApi alarmDatabaseApi;
  private Provider<List<Alarm>> workManagerAlarmProvider;

  public void set(DatabaseAlarmApi alarmDatabaseApi) {
    this.alarmDatabaseApi = alarmDatabaseApi;
  }

  @Override
  public void set2(Provider<List<Alarm>> workManagerAlarmProvider) {
    this.workManagerAlarmProvider = workManagerAlarmProvider;
  }

  @Override
  public AsyncAlarmRepoApi get() {
    if (alarmRepoApi == null) {
      alarmRepoApi = new AlarmRepoFactory(alarmDatabaseApi, workManagerAlarmProvider)
          .createAsyncRepo();
    }
    return alarmRepoApi;
  }
}
