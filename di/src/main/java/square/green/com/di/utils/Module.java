package square.green.com.di.utils;

public interface Module<T> {

    T get();

}
