package square.green.com.di.module;

import android.app.Activity;
import android.content.Context;
import square.green.com.di.utils.Module;
import square.green.com.di.utils.ModuleSetter;
import square.green.com.di.utils.ModuleSetter2;
import square.green.com.utils.api.alarmrepo.AsyncAlarmRepoApi;
import task.job.example.utils.api.AlarmWorkApi;


public class AlarmWorkerModuleDI<T extends Activity> implements Module<AlarmWorkApi>,
    ModuleSetter<AsyncAlarmRepoApi>, ModuleSetter2<Context> {

  private AlarmWorkApi alarmWorkApi;
  private AsyncAlarmRepoApi alarmRepoApi;
  private Context context;

  @Override
  public void set(AsyncAlarmRepoApi alarmRepoApi) {
    this.alarmRepoApi = alarmRepoApi;
  }

  @Override
  public void set2(Context value) {
    this.context = value;
  }

  @Override
  public AlarmWorkApi get() {
    if (alarmWorkApi == null) {
      //alarmWorkApi = new AlarmWorkFactory(context, alarmRepoApi).provideAppAlarmManager();
    }
    return alarmWorkApi;
  }
}
