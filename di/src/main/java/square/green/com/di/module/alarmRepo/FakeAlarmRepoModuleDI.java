package square.green.com.di.module.alarmRepo;


import square.green.com.utils.api.alarmrepo.AsyncAlarmRepoApi;
import square.green.com.utils.api.alarmrepo.AsyncAlarmRepoApi.EmptyAsyncAlarmRepo;

public class FakeAlarmRepoModuleDI extends AlarmRepoModuleDI {

  @Override
  public AsyncAlarmRepoApi get() {
    return new EmptyAsyncAlarmRepo();
  }
}
