package square.green.com.di.module;

import alarmwork.example.job.task.alarmwork_api.AlarmWorkLiveFactory;
import square.green.com.di.utils.Module;
import task.job.example.utils.api.AlarmWorkLiveApi;


public class AlarmWorkerLiveModuleDI implements Module<AlarmWorkLiveApi> {

    private AlarmWorkLiveApi alarmWorkLiveApi;

    public AlarmWorkLiveApi get() {
        if(alarmWorkLiveApi == null)
            alarmWorkLiveApi = new AlarmWorkLiveFactory().provideAlarmWorkLiveDataApi();
        return alarmWorkLiveApi;
    }
}
