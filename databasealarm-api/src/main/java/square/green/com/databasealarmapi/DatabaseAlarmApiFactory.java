package square.green.com.databasealarmapi;


import square.green.com.databasewithfile.DatabaseUsingFileManager;
import square.green.com.utils.api.databasealarm.DatabaseAlarmApi;

public class DatabaseAlarmApiFactory implements DatabaseAlarmApiLib {

  private final String path;

  public DatabaseAlarmApiFactory(String path) {
    this.path = path;
  }

  @Override
  public DatabaseAlarmApi createDatabase() {
    return DatabaseUsingFileManager.getInstance(path, false);
  }

}
