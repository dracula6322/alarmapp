package square.green.com.databasealarmapi;

import square.green.com.utils.api.databasealarm.DatabaseAlarmApi;

interface DatabaseAlarmApiLib {

  DatabaseAlarmApi createDatabase();
}
