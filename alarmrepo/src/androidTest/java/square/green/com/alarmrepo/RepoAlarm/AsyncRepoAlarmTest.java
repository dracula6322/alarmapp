package square.green.com.alarmrepo.RepoAlarm;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.Test;
import org.junit.runner.RunWith;
import square.green.com.utils.Provider;
import square.green.com.utils.api.alarmrepo.AsyncAlarmRepoApi;
import square.green.com.utils.api.databasealarm.DatabaseAlarmApi;
import square.green.com.utils.stub.FakeAlarmDatabaseWithValue;
import task.job.example.utils.model.Alarm;
import task.job.example.utils.model.AlarmBuilder;

@RunWith(AndroidJUnit4.class)
public class AsyncRepoAlarmTest {

  @Test
  public void test1() {

    ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(1);
    ExecutorService threadPoolExecutor2 = Executors.newFixedThreadPool(1);

    for (int i = 0; i < 10000; i++) {

      Log.d("TAG", "test1: " + i);

      DatabaseAlarmApi databaseAlarmApiStub = new FakeAlarmDatabaseWithValue(
          Collections.emptyList());
      Provider<List<Alarm>> workProvider = new Provider<List<Alarm>>() {
        @NonNull
        @Override
        public List<Alarm> getValue() {
          ArrayList<Alarm> result = new ArrayList<>();
          result.add(AlarmBuilder.generateDefaultAlarmBuilder(101).build());
          return result;
        }
      };

      AsyncAlarmRepoApi asyncAlarmRepoApi = new AsyncRepoAlarm(databaseAlarmApiStub, workProvider);

      CountDownLatch countDownLatch = new CountDownLatch(2);

      final boolean[] firstResult = new boolean[1];
      threadPoolExecutor.execute(new Runnable() {
        @Override
        public void run() {
          Log.e("TAG", "test1: threadPoolExecutor start");
          firstResult[0] = asyncAlarmRepoApi.deleteAlarmById(101) == null;
          Log.e("TAG", "test1: threadPoolExecutor middle");
          countDownLatch.countDown();
          Log.e("TAG", "test1: threadPoolExecutor end");
        }
      });

      final boolean[] secondResult = new boolean[1];
      threadPoolExecutor2.execute(new Runnable() {
        @Override
        public void run() {
          Log.e("TAG", "test1: threadPoolExecutor2 start");
          secondResult[0] = asyncAlarmRepoApi.deleteAlarmById(101) == null;
          Log.e("TAG", "test1: threadPoolExecutor2 middle");
          countDownLatch.countDown();
          Log.e("TAG", "test1: threadPoolExecutor2 end");
        }
      });

      try {
        countDownLatch.await();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      Log.d("TAG", "test1: " + secondResult[0] + " " + firstResult[0]);
      if (!secondResult[0] && !firstResult[0]) {
        Log.e("TAG", "test1: Hello");
      }

      if (secondResult[0] && firstResult[0]) {
        Log.e("TAG", "test1: Hello # 2");
      }

    }


  }


}