package square.green.com.alarmrepo.RepoAlarm;

import androidx.annotation.NonNull;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import square.green.com.utils.Provider;
import square.green.com.utils.api.alarmrepo.AsyncAlarmRepoApi;
import square.green.com.utils.api.alarmrepo.ResponseRepo;
import square.green.com.utils.api.databasealarm.DatabaseAlarmApi;
import task.job.example.utils.BackgroundThread;
import task.job.example.utils.model.Alarm;

public class AsyncRepoAlarm extends RepoAlarm implements AsyncAlarmRepoApi {

  private static volatile BackgroundThread repoThread = new BackgroundThread("AlarmRepoThread");
  private CountDownLatch constructionLock = new CountDownLatch(1);
  private static volatile AsyncRepoAlarm asyncInstance;
  private DatabaseAlarmApi databaseAlarmApi;

  public static AsyncRepoAlarm getInstance(@NonNull DatabaseAlarmApi alarmDatabaseApi,
      @NonNull Provider<List<Alarm>> alarmWorkApi) {
    AsyncRepoAlarm localInstance = asyncInstance;
    if (localInstance == null) {
      synchronized (AsyncRepoAlarm.class) {
        localInstance = asyncInstance;
        if (localInstance == null) {
          Instance = localInstance = new AsyncRepoAlarm(alarmDatabaseApi, alarmWorkApi);
        }
      }
    }
    return localInstance;
  }

  public AsyncRepoAlarm() {
  }

  public AsyncRepoAlarm(DatabaseAlarmApi alarmDatabaseApi,
      @NonNull Provider<List<Alarm>> alarmWorkApi) {

    this.databaseAlarmApi = alarmDatabaseApi;

    List<Alarm> massAlarmDatabaseApi = alarmDatabaseApi.getAlarms();
    List<Alarm> massAlarmWorkApi = alarmWorkApi.getValue();

    setAlarms(super.combineAlarmsFromTwoSource(massAlarmWorkApi, massAlarmDatabaseApi));

    constructionLock.countDown();
  }

  @Override
  public boolean writeAlarm(@NonNull Alarm alarm) {
    databaseAlarmApi.writeAlarm(alarm);
    return getAlarms().add(alarm);
  }

  @Override
  public void deleteAlarmById(long id, ResponseRepo<Alarm> response) {
    if (waitUntilConstructorIsNotHasBeenWorked()) {
      return;
    }
    databaseAlarmApi.deleteAlarm(id);

    repoThread.postRunnable(() -> {
      synchronized (getAlarms()) {
        response.getResult(AsyncRepoAlarm.super.deleteAlarmById(id));
      }
    });
  }

  @Override
  public void getAlarmById(long id, ResponseRepo<Alarm> response) {
    if (waitUntilConstructorIsNotHasBeenWorked()) {
      return;
    }
    repoThread.postRunnable(() -> response.getResult(super.getAlarmById(id)));
  }


  @Override
  public void getAlarms(ResponseRepo<List<Alarm>> response) {
    if (waitUntilConstructorIsNotHasBeenWorked()) {
      return;
    }
    repoThread.postRunnable(() -> response.getResult(super.getAlarms()));
  }

  private boolean waitUntilConstructorIsNotHasBeenWorked() {

    try {
      constructionLock.await();
      return false;
    } catch (InterruptedException e) {
      e.printStackTrace();
      return true;
    }
  }

  @Override
  public void clearThread() {
    if (waitUntilConstructorIsNotHasBeenWorked()) {
      return;
    }
    repoThread.stopThread();
  }

}
