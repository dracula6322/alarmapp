package square.green.com.alarmrepo.RepoAlarm;

import static task.job.example.utils.TimeUtils.SLIDER_TYPE;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import square.green.com.utils.api.alarmrepo.AlarmRepoApi;
import task.job.example.utils.model.Alarm;

public class RepoAlarm implements AlarmRepoApi {

  private List<Alarm> alarms = new ArrayList<>();
  static volatile RepoAlarm Instance;

  public static RepoAlarm getInstance() {
    RepoAlarm localInstance = Instance;
    if (localInstance == null) {
      synchronized (RepoAlarm.class) {
        localInstance = Instance;
        if (localInstance == null) {
          Instance = localInstance = new RepoAlarm();
        }
      }
    }
    return localInstance;
  }

  @NonNull
  public ArrayList<Alarm> combineAlarmsFromTwoSource(@NonNull List<Alarm> firstSource,
      @NonNull List<Alarm> secondSource) {

    ArrayList<Alarm> alarms = new ArrayList<>(firstSource);

    for (Alarm alarm : secondSource) {

      Alarm currentAlarm = null;

      for (Alarm internalAlarm : alarms) {
        if (internalAlarm.getId() == alarm.getId()) {
          currentAlarm = internalAlarm;
          break;
        }
      }

      if (currentAlarm != null) {
        currentAlarm.setVibrate(alarm.isVibrate());
        currentAlarm.setRingtone(alarm.getRingtone());
        currentAlarm.setAnswer(alarm.getAnswer());

      } else {
        alarm.setVibrate(true);             // Set default values
        alarm.setRingtone("");
        alarm.setAnswer("");
        alarm.setAnswerType(SLIDER_TYPE);
        alarms.add(alarm);

      }
    }

    return alarms;
  }


  @Nullable
  @Override
  public Alarm getAlarmById(long id) {

    for (Alarm alarm : alarms) {
      if (alarm.getId() == id) {
        return alarm;
      }
    }
    return null;
  }

  @Nullable
  @Override
  public Alarm deleteAlarmById(long id) {
    try {
      Log.e("TAG", "test1: we are in deleteAlarmById " + Thread.currentThread().getName());
      for (Alarm alarm : alarms) {
        Log.e("TAG",
            "test1: we are exit in for deleteAlarmById" + Thread.currentThread().getName());

        if (alarm.getId() == id) {
          alarms.remove(alarm);
          return alarm;
        }
      }
      Log.e("TAG", "test1: we are exit deleteAlarmById" + Thread.currentThread().getName());
      return null;
    } catch (Exception e) {
      Log.v("TAG", e.toString());
    }
    return null;
  }

  @Override
  public boolean writeAlarm(@NonNull Alarm alarm) {
    return alarms.add(alarm);
  }

  @NonNull
  @Override
  public List<Alarm> getAlarms() {
    return alarms;
  }

  protected void setAlarms(List<Alarm> alarms) {
    this.alarms = alarms;
  }

  public static void clearInstance() {
    Instance = null;
  }

}
