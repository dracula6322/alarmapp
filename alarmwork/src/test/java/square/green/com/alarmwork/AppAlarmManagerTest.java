package square.green.com.alarmwork;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static task.job.example.utils.api.AlarmWorkApi.ALARM_ID_L;
import static task.job.example.utils.api.AlarmWorkApi.ALARM_TYPE_I;
import static task.job.example.utils.api.AlarmWorkApi.TIME_TRIGGER_ALARM_L;

import alarmwork.example.job.task.alarmwork.AlarmExerciseManager;
import alarmwork.example.job.task.alarmwork.AppAlarmManager;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import task.job.example.utils.response.CreateAlarmReport;

public class AppAlarmManagerTest {

  @Test
  public void checkCorrectCreateDataInSetAlarm() {

    int alarmType = 102;
    int alarmId = 101;
    long launchTimeInSeconds = System.currentTimeMillis();

    CreateAlarmReport alarmReport = AppAlarmManager.getInstance(null, null,
        AlarmExerciseManager.getInstance())
        .setAlarm(alarmType, 0, 1, 0, 0, 0, 6, alarmId, launchTimeInSeconds);

    assertFalse(alarmReport.isResult());
    assertEquals(alarmReport.getData().getInt(ALARM_TYPE_I, -1), alarmType);
    assertEquals(alarmReport.getData().getLong(ALARM_ID_L, -1), alarmId);

    long triggerTime = alarmReport.getData().getLong(TIME_TRIGGER_ALARM_L, -1);

    long delta = triggerTime - launchTimeInSeconds;

    checkTwoCalendar(delta, 0, 23, 6);
  }

  private void checkTwoCalendar(long nearDay, int day, int hour,
      int minute) {

    Calendar secondCalendar = new GregorianCalendar();
    secondCalendar.setTimeInMillis(nearDay);

    int differenceDays = (int) TimeUnit.MILLISECONDS.toDays(nearDay);

    assertEquals(differenceDays, day);

    int differenceHours = (int) (TimeUnit.MILLISECONDS.toHours(nearDay) - 24 * differenceDays);

    assertEquals(differenceHours, hour);

    int differenceMinutes = (int) (
        TimeUnit.MILLISECONDS.toMinutes(nearDay) - (differenceDays * 24 + differenceHours) * 60);

    assertEquals(differenceMinutes, minute);

    assertEquals(secondCalendar.get(Calendar.SECOND), 0);

    assertEquals(secondCalendar.get(Calendar.MILLISECOND), 0);

  }

}