package square.green.com.alarmwork.worker;

import static task.job.example.utils.api.AlarmWorkApi.ALARM_ID_L;

import alarmwork.example.job.task.alarmwork.AlarmExerciseManager;
import alarmwork.example.job.task.alarmwork.AppAlarmManager;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import square.green.com.utils.api.alarmrepo.AsyncAlarmRepoApi;
import task.job.example.utils.FileLog;
import task.job.example.utils.response.CreateAlarmReport;


public class ScheduleAlarmWorker extends Worker {

  public static AsyncAlarmRepoApi asyncAlarmRepoApi;
  public static String NEW_CREATED_ALARM_TIME_L = "NEW_CREATED_ALARM_TIME_L";

  public ScheduleAlarmWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
    super(context, workerParams);
  }

  @NonNull
  @Override
  public Result doWork() {
    FileLog.getInstance().v("do Schedule Work");

    final long idAlarm = getInputData().getLong(ALARM_ID_L, -1);

    AtomicReference<Result> result = new AtomicReference<>(Result.failure());

    if (idAlarm == -1) {
      result.set(Result.failure());
      return result.get();
    }

    CountDownLatch countDownLatch = new CountDownLatch(1);
    asyncAlarmRepoApi.getAlarmById(idAlarm, alarmFromRepo -> {

      if (alarmFromRepo == null) {
        countDownLatch.countDown();
        return;
      }

      Calendar launchCalendar = Calendar.getInstance();
      launchCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
      launchCalendar.setTimeInMillis(System.currentTimeMillis());

      int currentDays = launchCalendar.get(Calendar.DAY_OF_WEEK);
      int currentHour = launchCalendar.get(Calendar.HOUR_OF_DAY);
      int currentMinute = launchCalendar.get(Calendar.MINUTE);

      if (alarmFromRepo.getDays() != 0) {
        CreateAlarmReport report = AppAlarmManager.getInstance(null, null,
            AlarmExerciseManager.getInstance())
            .setAlarm(alarmFromRepo.getAnswerType(), currentDays, currentMinute, currentHour,
                alarmFromRepo.getMinute(),
                alarmFromRepo.getHour(),
                alarmFromRepo.getDays(), alarmFromRepo.getId(),
                System.currentTimeMillis());

        if (report != null) {

          Data data = new Data.Builder()
              .putLong(NEW_CREATED_ALARM_TIME_L, report.getTime())
              .build();
          result.set(Result.success(data));
        }
      }

      countDownLatch.countDown();
    });
    try {
      countDownLatch.await();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return result.get();
  }

}
