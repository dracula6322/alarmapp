package square.green.com.alarmwork;

import static task.job.example.utils.TimeUtils.SLIDER_TYPE;
import static task.job.example.utils.api.AlarmWorkApi.ALARM_DAYS_I;
import static task.job.example.utils.api.AlarmWorkApi.ALARM_ID_L;
import static task.job.example.utils.api.AlarmWorkApi.ALARM_TYPE_I;
import static task.job.example.utils.api.AlarmWorkApi.TIME_TRIGGER_ALARM_L;
import static task.job.example.utils.model.Alarm.ALARM_OFF;
import static task.job.example.utils.model.Alarm.ALARM_ON;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import square.green.com.utils.Provider;
import task.job.example.utils.model.Alarm;
import task.job.example.utils.model.AlarmBuilder;

public class AlarmProviderFromWorkManager implements Provider<List<Alarm>> {

  private static volatile AlarmProviderFromWorkManager Instance;
  private WorkManager workManager;

  public static AlarmProviderFromWorkManager getInstance(@NonNull Context context) {
    AlarmProviderFromWorkManager localInstance = Instance;
    if (localInstance == null) {
      synchronized (AlarmProviderFromWorkManager.class) {
        localInstance = Instance;
        if (localInstance == null) {
          Instance = localInstance = new AlarmProviderFromWorkManager(context);
        }
      }
    }
    return localInstance;
  }

  private AlarmProviderFromWorkManager(Context context) {

    try {
      workManager = WorkManager.getInstance(context);
    } catch (IllegalStateException e) {
      e.printStackTrace();
      workManager = null;
    }
  }

  @NonNull
  @Override
  public List<Alarm> getValue() {

    List<WorkInfo> alarms = getAlarmWorks();
    List<Alarm> alarmForLists = new ArrayList<>();
    for (WorkInfo alarm : alarms) {

      long id = alarm.getOutputData().getLong(ALARM_ID_L, -1);

      Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
      calendar.setTimeInMillis(alarm.getOutputData().getLong(TIME_TRIGGER_ALARM_L, -1));

      int hour = calendar.get(Calendar.HOUR_OF_DAY);
      int minute = calendar.get(Calendar.MINUTE);

      int days = alarm.getOutputData().getInt(ALARM_DAYS_I, -1);
      int state = alarm.getState().isFinished() ? ALARM_OFF : ALARM_ON;
      long time = alarm.getOutputData().getLong(TIME_TRIGGER_ALARM_L, -1);
      int type = alarm.getOutputData().getInt(ALARM_TYPE_I, SLIDER_TYPE);

      Alarm currentAlarm = AlarmBuilder.anAlarm()
          .withState(state)
          .withId(id)
          .withTime(time)
          .withAnswerType(type)
          .withDays(days)
          .withHour(hour)
          .withMinute(minute)
          .build();

      alarmForLists.add(currentAlarm);
    }

    return alarmForLists;
  }

  @NonNull
  private List<WorkInfo> getAlarmWorks() {

    try {
      return workManager.getWorkInfosByTag("Alarm").get();
    } catch (ExecutionException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    return Collections.emptyList();
  }

}
