package square.green.com.alarmwork.worker;

import static task.job.example.utils.api.AlarmWorkApi.ALARM_TYPE_I;
import static task.job.example.utils.api.AlarmWorkApi.TIME_TRIGGER_ALARM_L;

import alarmwork.example.job.task.alarmwork.TimeController;
import android.content.Context;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import square.green.com.utils.api.alarmrepo.AsyncAlarmRepoApi;
import task.job.example.utils.api.AlarmWorkLiveApi;

public class PlayAlarmWorker extends Worker {

  public static String WRONG_INPUT_DATA_S = "WRONG_INPUT_DATA_S";
  public static String LATE_ALARM_S = "LATE_ALARM_S";
  public static String ALARM_NOT_FOUND_S = "ALARM_NOT_FOUND_S";
  public static String DATABASE_IS_NOT_ADDED_S = "DATABASE_IS_NOT_ADDED_S";
  public static String OTHER_ALARM_IS_PLAYING_IN_CURRENT_TIME_S = "OTHER_ALARM_IS_PLAYING_IN_CURRENT_TIME_S";
  public static String ERROR_IN_TRY_BLOCK_IN_COUNTDOWN_WAIT_S = "ERROR_IN_TRY_BLOCK_IN_COUNTDOWN_WAIT_S";
  public static String ERROR_DESC = "ERROR_DESC";

  static final long alarmLateTime = 60000;

  static AsyncAlarmRepoApi asyncAlarmRepoApi;
  public static AlarmWorkLiveApi alarmWorkLiveApi;

  public PlayAlarmWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
    super(context, workerParams);
  }

  @NonNull
  @Override
  public Result doWork() {

    Toast.makeText(getApplicationContext(), "Будильник сработал", Toast.LENGTH_LONG).show();

    AtomicReference<Result> workResult = new AtomicReference<>(Result.failure());

    final long idAlarm = 55;
    final long time = getInputData().getLong(TIME_TRIGGER_ALARM_L, -1);

    if (asyncAlarmRepoApi == null) {
      workResult.set(Result.failure(
          new Data.Builder().putString(ERROR_DESC, DATABASE_IS_NOT_ADDED_S).putAll(getInputData())
              .build()));
      return workResult.get();
    }

    if (idAlarm == -1 || time == -1) {
      workResult.set(Result.failure(
          new Data.Builder().putString(ERROR_DESC, WRONG_INPUT_DATA_S).putAll(getInputData())
              .build()));
      return workResult.get();
    }

    if (Math.abs(time - TimeController.getInstance().getCurrentTime()) > alarmLateTime) {
      workResult.set(
          Result.failure(new Data.Builder().putString(ERROR_DESC, LATE_ALARM_S).build()));
      return workResult.get();
    }

    CountDownLatch countDownLatch = new CountDownLatch(1);
    asyncAlarmRepoApi.getAlarmById(idAlarm, alarmFromRepo -> {

      if (alarmFromRepo == null) {
        workResult.set(Result.failure(new Data.Builder().putString(ERROR_DESC, ALARM_NOT_FOUND_S)
            .build()));
        countDownLatch.countDown();
        return;
      }

      boolean result = alarmWorkLiveApi.startAlarmService(getApplicationContext(), alarmFromRepo,
          getInputData().getInt(ALARM_TYPE_I, -1));
      if (!result) {
        workResult.set(Result.failure(
            new Data.Builder().putString(ERROR_DESC, OTHER_ALARM_IS_PLAYING_IN_CURRENT_TIME_S)
                .build()));
        countDownLatch.countDown();
        return;
      }

      workResult.set(Result.success());
      countDownLatch.countDown();
    });

    try {
      countDownLatch.await();
    } catch (InterruptedException e) {
      workResult.set(Result.failure(
          new Data.Builder().putString(ERROR_DESC, ERROR_IN_TRY_BLOCK_IN_COUNTDOWN_WAIT_S)
              .putString("ExceptionDesc", e.toString())
              .build()));
    }
    return workResult.get();
  }


}
