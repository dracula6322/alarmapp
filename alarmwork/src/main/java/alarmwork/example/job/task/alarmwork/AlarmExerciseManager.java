package alarmwork.example.job.task.alarmwork;

import static square.green.com.alarmwork.PlayAlarmService.ACTION_FOO;
import static square.green.com.alarmwork.PlayAlarmService.EXTRA_PARAM1;
import static square.green.com.alarmwork.PlayAlarmService.EXTRA_PARAM2;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import square.green.com.alarmwork.PlayAlarmService;
import square.green.com.alarmwork.WorkingLiveAlarm;
import square.green.com.alarmwork.media.RingtonePlayer;
import square.green.com.alarmwork.media.VibrationPlayer;
import task.job.example.utils.api.AlarmWorkLiveApi;
import task.job.example.utils.model.Alarm;

public class AlarmExerciseManager implements AlarmWorkLiveApi {

  private WorkingLiveAlarm workingLiveAlarm = WorkingLiveAlarm.getInstance();
  private RingtonePlayer ringtonePlayer = RingtonePlayer.getInstance();
  private VibrationPlayer vibrationPlayer = VibrationPlayer.getInstance();

  private static volatile AlarmExerciseManager Instance;

  public static AlarmExerciseManager getInstance() {
    AlarmExerciseManager localInstance = Instance;
    if (localInstance == null) {
      synchronized (AlarmExerciseManager.class) {
        localInstance = Instance;
        if (localInstance == null) {
          Instance = localInstance = new AlarmExerciseManager();
        }
      }
    }
    return localInstance;
  }

  @Override
  public boolean startAlarmService(Context context, Alarm alarm, int type) {

    if (isWorkingAlarmInCurrentTime()) {
      return false;
    }

    ComponentName result = startPlayAlarmService(context, alarm, type);
    if (result == null) {
      return false;
    }
    workingLiveAlarm.setWorkingAlarm(alarm.getId());
    return true;
  }

  public void startPlayingAlarm(Context context, boolean isVibrate, String ringtone) {

    if (!isWorkingAlarmInCurrentTime()) {
      return;
    }

    if (isVibrate) {
      vibrationPlayer.playVibrate(context);
    }
    ringtonePlayer.playAlarmRingtone(context, ringtone);
  }

  public void stopPlayingAlarm(@NonNull Context context) {

    if (!isWorkingAlarmInCurrentTime()) {
      return;
    }

    ringtonePlayer.stopPlayRingtone(context);
    vibrationPlayer.stopPlayVibration();
    workingLiveAlarm.setWorkingAlarm(-1L);
  }

  private boolean isWorkingAlarmInCurrentTime() {
    return workingLiveAlarm.isWorkingAlarmInCurrentTime();
  }

  @Override
  public MutableLiveData<Long> getLiveDataWorking() {
    return workingLiveAlarm.getWorkingAlarm();
  }

  @Override
  public boolean stopAlarmService(Context context) {
    Intent cancelAlarm = new Intent(context, PlayAlarmService.class);
    return context.stopService(cancelAlarm);
  }


  @Nullable
  private ComponentName startPlayAlarmService(@NonNull Context context, @NonNull Alarm alarm,
      int type) {
    Intent intent = new Intent(context, PlayAlarmService.class);
    intent.setAction(ACTION_FOO);
    intent.putExtra(EXTRA_PARAM1, alarm);
    intent.putExtra(EXTRA_PARAM2, type);

    ComponentName result;

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      result = context.startForegroundService(intent);
    } else {
      result = context.startService(intent);
    }
    return result;
  }

}
