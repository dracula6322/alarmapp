package alarmwork.example.job.task.alarmwork;

import android.content.Context;
import android.content.Intent;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.Operation;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import square.green.com.alarmwork.PlayAlarmService;
import square.green.com.alarmwork.worker.PlayAlarmWorker;
import square.green.com.alarmwork.worker.ScheduleAlarmWorker;
import square.green.com.utils.api.alarmrepo.AlarmRepoApi;
import square.green.com.utils.api.alarmrepo.AsyncAlarmRepoApi;
import task.job.example.utils.TimeUtils;
import task.job.example.utils.api.AlarmWorkApi;
import task.job.example.utils.api.AlarmWorkLiveApi;
import task.job.example.utils.model.Alarm;
import task.job.example.utils.response.CreateAlarmReport;

public class AppAlarmManager implements AlarmWorkApi {

  public static volatile Context context;
  private static volatile AppAlarmManager Instance;
  private WorkManager workManager;

  public static void setupEnvironment(Context context, AlarmRepoApi repoAlarmDao,
      AlarmWorkLiveApi alarmWorkLiveApi) {
  }

  public static AppAlarmManager getInstance(Context context, AsyncAlarmRepoApi repoAlarmDao,
      AlarmWorkLiveApi alarmWorkLiveApi) {
    AppAlarmManager localInstance = Instance;
    if (localInstance == null) {
      synchronized (AppAlarmManager.class) {
        localInstance = Instance;
        if (localInstance == null) {
          Instance = localInstance = new AppAlarmManager(context, repoAlarmDao, alarmWorkLiveApi);
        }
      }
    }
    return localInstance;
  }

  private AppAlarmManager(Context context, AsyncAlarmRepoApi repoAlarmDao,
      AlarmWorkLiveApi alarmWorkLiveApi) {

    PlayAlarmWorker.alarmWorkLiveApi = alarmWorkLiveApi;
    ScheduleAlarmWorker.asyncAlarmRepoApi = repoAlarmDao;

    try {
      workManager = WorkManager.getInstance(context);
    } catch (IllegalStateException e) {
      workManager = null;
    }
  }

  @Override
  public CreateAlarmReport onAlarmFromCurrentTime(Alarm alarm) {

    Calendar launchCalendar = Calendar.getInstance();
    launchCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
    launchCalendar.setTimeInMillis(System.currentTimeMillis());

    int currentDays = launchCalendar.get(Calendar.DAY_OF_WEEK);
    int currentHour = launchCalendar.get(Calendar.HOUR_OF_DAY);
    int currentMinute = launchCalendar.get(Calendar.MINUTE);

    return setAlarm(alarm.getAnswerType(), currentDays, currentHour, currentMinute, alarm.getDays(),
        alarm.getHour(), alarm.getMinute(), alarm.getId(),
        launchCalendar.getTime().getTime());


  }

  @Override
  public void cancelAlarm(long id) {
    workManager.cancelUniqueWork(String.valueOf(id));
  }


  void enqueueOneTimeWorkRequest(WorkRequest oneTimeWorkRequest) {

    try {
      workManager.enqueue(oneTimeWorkRequest).getResult().get();
    } catch (ExecutionException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public WorkInfo getWorkInfoById(WorkRequest oneTimeWorkRequest) {

    WorkInfo workInfo = null;

    ListenableFuture<WorkInfo> workInfoListenableFuture = workManager
        .getWorkInfoById(oneTimeWorkRequest.getId());

    try {
      workInfo = workInfoListenableFuture.get();
    } catch (ExecutionException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    return workInfo;
  }

  @Override
  public void stopService(Context context) {
    Intent cancelAlarm = new Intent(context, PlayAlarmService.class);
    context.stopService(cancelAlarm);
  }

  OneTimeWorkRequest createPlayAlarmWorker(long delay, Data data) {

    return new OneTimeWorkRequest.Builder(PlayAlarmWorker.class)
        .setInitialDelay(delay, TimeUnit.MILLISECONDS)
        .setInputData(data)
        .addTag("Alarm")
        .build();
  }

  OneTimeWorkRequest createScheduleAlarmWorker(String alarmWorkerId, Data data) {

    return new OneTimeWorkRequest.Builder(ScheduleAlarmWorker.class)
        .setInputData(data)
        .addTag(alarmWorkerId)
        .addTag("Scheduler")
        .build();
  }

  Operation enqueueWorkRequestAndScheduler(long idAlarm, OneTimeWorkRequest workRequest,
      OneTimeWorkRequest schedulerRequest) {

    if (workManager == null) {
      return null;
    }
    return workManager
        .beginUniqueWork(String.valueOf(idAlarm), ExistingWorkPolicy.REPLACE, workRequest)
        .then(schedulerRequest)
        .enqueue();
  }

  private CreateAlarmReport setAlarm(int type, long idAlarm, long delay, long triggerTime,
      int alarmDays) {

    Data data = createData(triggerTime, type, idAlarm, alarmDays);

    OneTimeWorkRequest playAlarm = createPlayAlarmWorker(delay, data);

    OneTimeWorkRequest scheduleAlarm = createScheduleAlarmWorker(playAlarm.getId().toString(),
        data);

    boolean result = true;
    if (enqueueWorkRequestAndScheduler(idAlarm, playAlarm, scheduleAlarm) == null) {
      result = false;
    }

    return new CreateAlarmReport(playAlarm.getId(), result, triggerTime, data);
  }

  private Data createData(long triggerTime, int type, long idAlarm, int alarmDays) {

    return new Data.Builder()
        .putInt(ALARM_TYPE_I, type)
        .putLong(ALARM_ID_L, idAlarm)
        .putLong(TIME_TRIGGER_ALARM_L, triggerTime)
        .putInt(ALARM_DAYS_I, alarmDays)
        .build();

  }

  @Override
  public CreateAlarmReport setAlarm(int type, int currentDays, int currentHour,
      int currentMinute, int alarmDays, int alarmHour, int alarmMinute, long idAlarm,
      long launchTime) {

    long delay = TimeUtils
        .getNearestDay(currentDays, currentHour, currentMinute, alarmDays, alarmHour,
            alarmMinute);

    long triggerTime = launchTime + delay;

    return setAlarm(type, idAlarm, delay, triggerTime, alarmDays);
  }

}
