package alarmwork.example.job.task.alarmwork;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;

public class TimeController {


  private static TimeController Instance;

  public static TimeController getInstance() {
    TimeController localInstance = Instance;
    if (localInstance == null) {
      synchronized (AppAlarmManager.class) {
        localInstance = Instance;
        if (localInstance == null) {
          Instance = localInstance = new TimeController();
        }
      }
    }
    return localInstance;
  }

  static public long getNearestDay(int hour, int minute, int days) {

    BitSet bitSet = new BitSet(7);

    bitSet.set(0, (days & 1) != 0);
    bitSet.set(1, (days & 2) != 0);
    bitSet.set(2, (days & 4) != 0);
    bitSet.set(3, (days & 8) != 0);
    bitSet.set(4, (days & 16) != 0);
    bitSet.set(5, (days & 32) != 0);
    bitSet.set(6, (days & 64) != 0);

    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, hour);
    calendar.set(Calendar.MINUTE, minute);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    long time = calendar.getTimeInMillis();
    ArrayList<Long> times = new ArrayList<>();
    times.add(time);
    times.add(time + 86400000);
    times.add(time + 86400000 * 2);
    times.add(time + 86400000 * 3);
    times.add(time + 86400000 * 4);
    times.add(time + 86400000 * 5);
    times.add(time + 86400000 * 6);
    times.add(time + 86400000 * 7);

    ArrayList<Boolean> results = new ArrayList<>();
    calendar = Calendar.getInstance();
    for (int i = 0; i < times.size(); i++) {
      results.add(!compareTo(calendar.getTime().getTime(), times.get(i)));
    }

    int bitwiseCount = calendar.get(Calendar.DAY_OF_WEEK);
    if (bitwiseCount == Calendar.SUNDAY) {
      bitwiseCount = 6;
    } else {
      bitwiseCount -= 2;
    }

    if (bitwiseCount != 0) {
      boolean tmp = bitSet.get(bitwiseCount);
      int previous = bitwiseCount;
      for (int i = 0; i < 6; i++) {
        int next = (previous + bitwiseCount) < 7 ? (previous + bitwiseCount)
            : (previous + bitwiseCount - 7);
        bitSet.set(previous, bitSet.get(next));
        previous = next;
      }
      bitSet.set(0, tmp);
    }

    if (bitSet.isEmpty()) {
      bitSet.set(0, true);
      bitSet.set(1, true);
    }

    long result = -1;
    for (int i = 0; i < 7; i++) {

      if (bitSet.get(i) && results.get(i)) {
        result = times.get(i);
        break;
      }
    }
    if (result == -1) {
      result = times.get(results.size() - 1);
    }

    return result + 8 * 1000;
  }

  public long getCurrentTime() {
    return System.currentTimeMillis();
  }

  public long getCurrentTimeMinusTwoSecond() {
    return System.currentTimeMillis() - 2 * 1000;
  }

  public long getCurrentTimePlusTwoSecond() {
    return System.currentTimeMillis() + 2 * 1000;
  }

  public long getCurrentTimePlusTwoMinute() {
    return System.currentTimeMillis() + 2 * 60 * 1000;
  }

  public long getCurrentTimePlusMillis(long time) {
    return System.currentTimeMillis() + time;
  }

  private static boolean compareTo(long currentTime, long t) {
    int result = Long.compare(currentTime, t);
    return result > 0;
  }

}
