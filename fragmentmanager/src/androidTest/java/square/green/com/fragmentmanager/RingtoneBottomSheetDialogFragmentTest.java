package square.green.com.fragmentmanager;


import android.content.Context;
import android.util.Log;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.rule.ActivityTestRule;
import java.util.concurrent.CountDownLatch;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import square.green.com.fragmentmanager.dialog.RingtoneBottomSheetDialogFragment;

public class RingtoneBottomSheetDialogFragmentTest {

  Context context;

  @Rule
  public ActivityTestRule<ApplicationTestFragmentActivity> activityFragmentRule
      = new ActivityTestRule<>(
      ApplicationTestFragmentActivity.class,
      true,     // initialTouchMode
      true);   // launchActivity. False to customize the intent

  @Before
  public void setUp() throws Exception {
    context = ApplicationProvider.getApplicationContext();
  }

  @Test
  public void testFragment() {

    final String[] ringtone = {""};

    activityFragmentRule.getActivity().findViewById(1).setOnClickListener(
        v -> {

          try {
            (new CountDownLatch(1)).await();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

          RingtoneBottomSheetDialogFragment launchFragment = RingtoneBottomSheetDialogFragment
              .newInstance(
                  ringtone[0],
                  string -> {

                    Log.v("TAG", "We get string " + string);
                    ringtone[0] = string;
                  });
          launchFragment
              .show(activityFragmentRule.getActivity().getSupportFragmentManager(), "ASD");
        });

    try {
      Thread.sleep(500000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

}
