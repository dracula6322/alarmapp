package square.green.com.fragmentmanager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import task.job.example.utils.SliderView;

public class SliderViewTestFragment extends AppCompatActivity {

  SliderView sliderView;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setTheme(R.style.AppTheme);
    sliderView = new SliderView(this);
    setContentView(sliderView);

    sliderView.setOnSlideCompleteListener(() -> {
      Toast.makeText(getApplicationContext(), "Press", Toast.LENGTH_SHORT).show();
      Log.d("TAG", "We sendMessage message");

      boolean result = LocalBroadcastManager
          .getInstance(getApplicationContext())
          .sendBroadcast(new Intent(Intent.ACTION_MAIN));
      Log.d("TAG", result + " !!!");

      setResult(RESULT_OK, new Intent().setAction("ASDASDASDASD"));


    });
  }

  public void pressSliderView() {
    sliderView.touchDownEvent(10, 10);
  }

  @Override
  protected void onStart() {
    super.onStart();

    new Thread(() -> {

      Log.d("TAG", "We are start thread" + " !!!");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      runOnUiThread(() -> {

        sliderView.touchDownEvent(10, 10);
        sliderView.touchMoveEvent(300);
        sliderView.touchUpEvent();
      });
    }).start();
  }

  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
  }
}
