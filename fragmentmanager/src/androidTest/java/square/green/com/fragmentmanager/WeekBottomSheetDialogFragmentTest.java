package square.green.com.fragmentmanager;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import android.content.Context;
import android.util.Log;
import androidx.core.util.Consumer;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.rule.ActivityTestRule;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import square.green.com.fragmentmanager.dialog.WeekBottomSheetDialogFragment;

public class WeekBottomSheetDialogFragmentTest {

  private Context context;

  @Before
  public void setup() {
    context = ApplicationProvider.getApplicationContext();
  }

  @After
  public void clear() {
  }

  @Rule
  public ActivityTestRule<ApplicationTestFragmentActivity> activityFragmentRule
      = new ActivityTestRule<>(
      ApplicationTestFragmentActivity.class,
      true,     // initialTouchMode
      true);   // launchActivity. False to customize the intent

  @Test
  public void testFragment() {

    WeekBottomSheetDialogFragment launchFragment = WeekBottomSheetDialogFragment.newInstance(333,
        new Consumer<Integer>() {
          @Override
          public void accept(Integer integer) {
            Log.v("TAG", integer.toString());
          }
        });

    activityFragmentRule.getActivity().getSupportFragmentManager()
        .beginTransaction()
        .replace(1, launchFragment)
        .commit();

    onView(withId(R.id.alarm_fragment_1)).perform(click());
    onView(withId(R.id.alarm_fragment_1)).check(matches(isDisplayed()));

    try {
      Thread.sleep(500000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

}
