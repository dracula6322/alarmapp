package square.green.com.fragmentmanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static task.job.example.utils.model.Alarm.ALARM_ON;

import android.app.Instrumentation.ActivityResult;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.rule.ActivityTestRule;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import square.green.com.di.ApplicationComponent;
import square.green.com.di.ApplicationComponent.Builder;
import square.green.com.di.ApplicationComponentImpl;
import square.green.com.di.module.AppModuleDI;
import square.green.com.di.utils.Module;
import square.green.com.di.utils.ModuleSetter;
import square.green.com.di.utils.ModuleSetter2;
import square.green.com.fragmentmanager.launchFragment.LaunchFragment;
import square.green.com.utils.Provider;
import square.green.com.utils.api.alarmrepo.AsyncAlarmRepoApi;
import square.green.com.utils.api.alarmrepo.AsyncAlarmRepoApi.EmptyAsyncAlarmRepo;
import square.green.com.utils.api.alarmrepo.ResponseRepo;
import square.green.com.utils.api.databasealarm.DatabaseAlarmApi;
import task.job.example.utils.api.AlarmWorkLiveApi;
import task.job.example.utils.model.Alarm;
import task.job.example.utils.model.AlarmBuilder;

@RunWith(AndroidJUnit4ClassRunner.class)
public class StartActivityTest {

  private Context context;

  @Before
  public void setup() {
    context = ApplicationProvider.getApplicationContext();
  }

  @After
  public void clear() {
  }

  @Rule
  public ActivityTestRule<SliderViewTestFragment> activityRule
      = new ActivityTestRule<>(
      SliderViewTestFragment.class,
      true,     // initialTouchMode
      false);   // launchActivity. False to customize the intent

  @Test
  public void intent() {

    for (int i = 0; i < 100; i++) {

      CountDownLatch countDownLatch = new CountDownLatch(1);

      BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
          Log.d("TAG", "We get message");
          countDownLatch.countDown();
        }
      };

      LocalBroadcastManager.getInstance(context).registerReceiver(mMessageReceiver,
          new IntentFilter(Intent.ACTION_MAIN));

      Intent intent = new Intent();
      intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      activityRule.launchActivity(intent);

      try {

        boolean result = countDownLatch.await(5, TimeUnit.SECONDS);
        assertTrue(result);

        LocalBroadcastManager.getInstance(context).unregisterReceiver(mMessageReceiver);

        activityRule.finishActivity();
        ActivityResult activityResult = activityRule.getActivityResult();

        assertEquals(activityResult.getResultData().getAction(), "ASDASDASDASD");

      } catch (InterruptedException e) {
        Log.d("TAG", e.toString());
        e.printStackTrace();
        throw new RuntimeException();
      }
    }

  }

  @Rule
  public ActivityTestRule<ApplicationTestFragmentActivity> activityFragmentRule
      = new ActivityTestRule<>(
      ApplicationTestFragmentActivity.class,
      true,     // initialTouchMode
      true);   // launchActivity. False to customize the intent

  @Test
  public void testFragment() {

    FakeAlarmWorkerLiveModuleDI fakeAlarmWorkerLiveModuleDI = new FakeAlarmWorkerLiveModuleDI();

    ApplicationComponent applicationComponent = new Builder()
        .contextModule(new AppModuleDI(context))
        .databaseModule(new StubDatabaseModuleDI())
        .alarmRepoModule(new StubAlarmRepoModuleDI())
        .alarmWorkerLiveModule(fakeAlarmWorkerLiveModuleDI)
        .build();

    ApplicationComponentImpl.getInstance().setApplicationComponent(applicationComponent);

    LaunchFragment launchFragment = new LaunchFragment();

    activityFragmentRule.getActivity().getSupportFragmentManager()
        .beginTransaction()
        .replace(1, launchFragment)
        .commit();

    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    activityFragmentRule.getActivity().runOnUiThread(
        () -> Toast.makeText(context, "We are here", Toast.LENGTH_SHORT).show());

    fakeAlarmWorkerLiveModuleDI.get().getLiveDataWorking().postValue(101L);

    try {
      Thread.sleep(500000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }


  public class StubDatabaseModuleDI implements Module<DatabaseAlarmApi>, ModuleSetter<Context> {

    private DatabaseAlarmApi alarmDatabaseApi = new DatabaseAlarmApi() {
      @Nullable
      @Override
      public Alarm getAlarmById(long alarmId) {
        return null;
      }

      @Override
      public boolean writeAlarm(@NonNull Alarm alarm) {
        return false;
      }

      @Override
      public int deleteAlarm(long alarmId) {
        return 0;
      }

      @NonNull
      @Override
      public List<Alarm> getAlarms() {
        return Collections.emptyList();
      }
    };
    private Context context;

    @Override
    public void set(Context context) {
      this.context = context;
    }

    @Override
    public DatabaseAlarmApi get() {
      return alarmDatabaseApi;
    }
  }


  public class StubAlarmRepoModuleDI implements Module<AsyncAlarmRepoApi>,
      ModuleSetter<DatabaseAlarmApi>, ModuleSetter2<Provider<List<Alarm>>> {

    private AsyncAlarmRepoApi alarmRepoApi = new EmptyAsyncAlarmRepo() {
      @Override
      public void deleteAlarmById(long id, @NonNull ResponseRepo<Alarm> response) {
        response.getResult(null);
      }

      @Override
      public void getAlarmById(long id, @NonNull ResponseRepo<Alarm> response) {
        response.getResult(null);
      }

      @Override
      public void getAlarms(@NonNull ResponseRepo<List<Alarm>> response) {

        response.getResult(
            Arrays.asList(
                AlarmBuilder.generateDefaultAlarmBuilder(100).build(),
                AlarmBuilder.generateDefaultAlarmBuilder(101).withState(ALARM_ON).build(),
                AlarmBuilder.generateDefaultAlarmBuilder(102).build(),
                AlarmBuilder.generateDefaultAlarmBuilder(103).build()
            )
        );
      }

      @Override
      public void clearThread() {

      }
    };

    public void set(DatabaseAlarmApi alarmDatabaseApi) {
    }

    @Override
    public void set2(Provider<List<Alarm>> value) {
    }

    @Override
    public AsyncAlarmRepoApi get() {
      return alarmRepoApi;
    }
  }

  public class FakeAlarmWorkerLiveModuleDI implements Module<AlarmWorkLiveApi> {

    private AlarmWorkLiveApi alarmWorkLiveApi = new AlarmWorkLiveApi() {

      MutableLiveData<Long> mutableLiveData = new MutableLiveData<>();

      @Override
      public boolean startAlarmService(Context context, Alarm alarmForService, int type) {
        return false;
      }

      @Override
      public MutableLiveData<Long> getLiveDataWorking() {
        return mutableLiveData;
      }

      @Override
      public boolean stopAlarmService(Context context) {
        return false;
      }
    };

    public AlarmWorkLiveApi get() {
      return alarmWorkLiveApi;
    }
  }

}