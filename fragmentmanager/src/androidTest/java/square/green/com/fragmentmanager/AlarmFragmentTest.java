package square.green.com.fragmentmanager;

import android.content.Context;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.rule.ActivityTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import square.green.com.di.ApplicationComponent;
import square.green.com.di.ApplicationComponent.Builder;
import square.green.com.di.ApplicationComponentImpl;
import square.green.com.di.module.AlarmWorkerLiveModuleDI;
import square.green.com.di.module.AlarmWorkerModuleDI;
import square.green.com.di.module.AppModuleDI;
import square.green.com.di.module.DatabaseModuleDI;
import square.green.com.di.module.alarmRepo.FakeAlarmRepoModuleDI;
import square.green.com.fragmentmanager.alarmFragment.AlarmFragmentFragment;

public class AlarmFragmentTest {

  private Context context;

  @Before
  public void setUp() {
    context = ApplicationProvider.getApplicationContext();
  }

  @Rule
  public ActivityTestRule<ApplicationTestFragmentActivity> activityFragmentRule
      = new ActivityTestRule<>(
      ApplicationTestFragmentActivity.class,
      true,     // initialTouchMode
      true);   // launchActivity. False to customize the intent

  @Test
  public void testFragmentWithNullParams() {

    ApplicationComponent applicationComponent = new Builder()
        .contextModule(new AppModuleDI(context))
        .databaseModule(new DatabaseModuleDI())
        .alarmRepoModule(new FakeAlarmRepoModuleDI())
        .alarmWorkerModule(new AlarmWorkerModuleDI())
        .alarmWorkerLiveModule(new AlarmWorkerLiveModuleDI())
        .build();

    ApplicationComponentImpl.getInstance().setApplicationComponent(applicationComponent);

    AlarmFragmentFragment alarmFragment = AlarmFragmentFragment.newInstance(null);
    activityFragmentRule.getActivity().getSupportFragmentManager().beginTransaction()
        .replace(1, alarmFragment).commit();

    try {
      Thread.sleep(500000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

}
