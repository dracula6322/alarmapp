package square.green.com.fragmentmanager;

import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import androidx.appcompat.app.AppCompatActivity;

public class ApplicationTestFragmentActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setTheme(R.style.AppTheme);

    LinearLayout view = new LinearLayout(this);
    view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    view.setId(1);

    setContentView(view);
  }

}
