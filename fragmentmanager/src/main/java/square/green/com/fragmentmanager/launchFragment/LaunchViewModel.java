package square.green.com.fragmentmanager.launchFragment;

import static task.job.example.utils.api.AlarmWorkApi.ALARM_ID_L;

import android.os.Bundle;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import square.green.com.di.ApplicationComponentImpl;
import task.job.example.utils.AlarmForList;
import task.job.example.utils.BackgroundThread;
import task.job.example.utils.TimeUtils;
import task.job.example.utils.api.AlarmWorkLiveApi;
import task.job.example.utils.response.CreateAlarmReport;

public class LaunchViewModel extends ViewModel implements LaunchPresenter {

  static final int LAUNCH_OPEN_ALARM_FRAGMENT_INTENT = 1;
  static final int DELETE_ALARM = 2;
  static final int ON_ALARM = 3;
  static final int OFF_ALARM = 4;

  private AlarmWorkLiveApi alarmWorkLiveApi = ApplicationComponentImpl.getInstance()
      .getApplicationComponent().provideAlarmWorkLiveApi();

  private volatile BackgroundThread backgroundThread = new BackgroundThread(
      "UpdateTimeInAlarmRecyclerView");

  LaunchFragmentReducer currentState = new LaunchFragmentReducer();
  LaunchRouter launchRouter = new LaunchRouter();


  public static LaunchPresenter buildLaunchViewModel(FragmentActivity activity,
      Class viewModelClass) {
    return (LaunchPresenter) ViewModelProviders.of(activity).get(viewModelClass);
  }


  public void connect(LaunchFragment launchFragment, AlarmRecyclerViewAdapter adapter) {

    launchFragment.getLifecycle().addObserver(new DefaultLifecycleObserver() {

      @Override
      public void onStart(@NonNull LifecycleOwner owner) {

        currentState.setLaunchView(launchFragment);
        launchRouter.setFragmentManager(launchFragment.getFragmentManager());
        currentState.addLaunchView(launchFragment.getLifecycle());
        adapter.startObserve(owner, alarmWorkLiveApi.getLiveDataWorking());
        startUpdateTimeThread();
      }

      @Override
      public void onStop(@NonNull LifecycleOwner owner) {

        currentState.setLaunchView(null);
        launchRouter.clearFragmentManager();
        launchFragment.getLifecycle().removeObserver(this);
        backgroundThread.stopThread();

      }
    });
  }

  @Override
  public void sendIntent(int value, Bundle bundle) {
    currentState.reduceEvent(value, bundle);
  }

  // New sector


  public void deleteAlarm(long id) {

    Bundle bundle = new Bundle();
    bundle.putLong(ALARM_ID_L, id);

    currentState.reduceEvent(DELETE_ALARM, bundle);
  }

  public void offAlarm(long id) {

    Bundle bundle = new Bundle();
    bundle.putLong(ALARM_ID_L, id);

    currentState.reduceEvent(OFF_ALARM, bundle);
  }

  public void onAlarm(AlarmForList alarm) {

    Bundle bundle = new Bundle();
    bundle.putParcelable("ALARM", alarm);

    CreateAlarmReport report = (CreateAlarmReport) currentState.reduceEvent(ON_ALARM, bundle);

    Toast
        .makeText(ApplicationComponentImpl.getInstance().getApplicationComponent().provideContext(),
            "Будильник сработает через " + TimeUtils.stringTimeToAlarm(report.getTime(), false),
            Toast.LENGTH_LONG).show();

  }

  private Runnable updateAdapterRunnable;

  void startUpdateTimeThread() {

    backgroundThread.stopThread();
    updateAdapterRunnable = () -> {

      boolean result = currentState.getLaunchView().updateTimeInHeaderRecyclerView();

      if (result && updateAdapterRunnable != null) {
        backgroundThread.postRunnable(updateAdapterRunnable, 3 * 1000);
      }

    };
    backgroundThread.postRunnable(updateAdapterRunnable);

  }

}
