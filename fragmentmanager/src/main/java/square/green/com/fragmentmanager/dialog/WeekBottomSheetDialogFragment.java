package square.green.com.fragmentmanager.dialog;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import java.util.Arrays;
import square.green.com.fragmentmanager.BuildConfig;
import square.green.com.fragmentmanager.DayTextView;
import square.green.com.fragmentmanager.R;

public class WeekBottomSheetDialogFragment extends BottomSheetDialogFragment {

  private static String TAG_DAYS_I = "TAG_DAYS_I";

  private final int[] idDays = new int[]{R.id.alarm_fragment_1,
      R.id.alarm_fragment_2,
      R.id.alarm_fragment_3,
      R.id.alarm_fragment_4,
      R.id.alarm_fragment_5,
      R.id.alarm_fragment_6,
      R.id.alarm_fragment_7};

  private final Consumer<Integer> emptyConsumer = integer -> {
  };
  private Consumer<Integer> daysConsumer = emptyConsumer;

  public static WeekBottomSheetDialogFragment newInstance(
      @IntRange(from = 0, to = 255) final int days,
      Consumer<Integer> consumer) {
    Bundle bundle = new Bundle();
    bundle.putInt(TAG_DAYS_I, days);
    WeekBottomSheetDialogFragment weekBottomSheetDialogFragment = new WeekBottomSheetDialogFragment();
    weekBottomSheetDialogFragment.setArguments(bundle);
    weekBottomSheetDialogFragment.daysConsumer = consumer;
    return weekBottomSheetDialogFragment;
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.week_dialog, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    if (getArguments() == null) {
      if (BuildConfig.DEBUG) {
        throw new RuntimeException("No arguments");
      }
    }

    final int[] oldDays = {getArguments().getInt(TAG_DAYS_I)};

    for (int i = 0; i < idDays.length; i++) {

      DayTextView day = view.findViewById(idDays[i]);
      day.setOnClickListener(view1 -> {

        int point = Arrays.binarySearch(idDays, view1.getId());
        if (point < 0) {
          return;
        }

        DayTextView day1 = (DayTextView) view1;

        oldDays[0] = (oldDays[0] ^ (int) Math.pow(2, point));

        if ((oldDays[0] & (int) Math.pow(2, point)) == 0) {
          day1.setTextColor(Color.parseColor("#bdbdbd"));
        } else {
          day1.setTextColor(Color.WHITE);
        }

        daysConsumer.accept(oldDays[0]);

      });

      if ((oldDays[0] & (int) Math.pow(2, i)) > 0) {
        day.setChosen(true);
        day.setTextColor(Color.WHITE);
      }
    }
  }
}
