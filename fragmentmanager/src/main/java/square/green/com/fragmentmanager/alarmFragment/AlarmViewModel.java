package square.green.com.fragmentmanager.alarmFragment;

import static square.green.com.fragmentmanager.alarmFragment.AlarmFragmentFragment.ALARM_ARGUMENT;
import static square.green.com.fragmentmanager.alarmFragment.AlarmFragmentReducer.SET_ANSWER_S_PARAM;
import static square.green.com.fragmentmanager.alarmFragment.AlarmFragmentReducer.SET_DAYS_I_PARAM;
import static square.green.com.fragmentmanager.alarmFragment.AlarmFragmentReducer.SET_RINGTONE_S_PARAM;
import static square.green.com.fragmentmanager.alarmFragment.AlarmFragmentReducer.SET_TYPE_I_PARAM;
import static square.green.com.fragmentmanager.alarmFragment.AlarmFragmentReducer.SET_VIBRATION_B_PARAM;
import static task.job.example.utils.TimeUtils.MATH_TYPE;

import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import java.lang.reflect.Constructor;
import java.util.Calendar;
import java.util.TimeZone;
import square.green.com.di.ApplicationComponentImpl;
import square.green.com.fragmentmanager.AlarmPresenter;
import square.green.com.fragmentmanager.MathFragment;
import task.job.example.utils.FileLog;
import task.job.example.utils.model.Alarm;

public class AlarmViewModel extends ViewModel implements AlarmPresenter {

  private AlarmBuilderScreenRouter alarmBuilderScreenRouter = new AlarmBuilderScreenRouter();
  private AlarmFragmentReducer alarmFragmentReducer = new AlarmFragmentReducer();

  static AlarmPresenter buildAlarmViewModel(Fragment fragment, Bundle arguments) {

    return ViewModelProviders
        .of(fragment, new AlarmViewModel.AlarmViewModelFactory(arguments))
        .get(AlarmViewModel.class);
  }

  private static class AlarmViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final Bundle arguments;

    AlarmViewModelFactory(@Nullable Bundle arguments) {
      this.arguments = arguments;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
      try {

        Constructor<T> constructor = modelClass.getConstructor(Bundle.class);
        return constructor.newInstance(arguments);
      } catch (Exception e) {
        e.printStackTrace();
        throw new RuntimeException(e);
      }
    }
  }

  public AlarmViewModel(Bundle arguments) {

    Alarm alarm = new Alarm();

    if (arguments != null && arguments.containsKey(ALARM_ARGUMENT)
        && arguments.getParcelable(ALARM_ARGUMENT) != null) {

      alarm = arguments.getParcelable(ALARM_ARGUMENT);

      alarmFragmentReducer.loadDataFromArgument(alarm);

      return;
    }

    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    alarm.setHour(calendar.get(Calendar.HOUR_OF_DAY));
    alarm.setMinute(calendar.get(Calendar.MINUTE));
    alarm.setDays(31);

    alarmFragmentReducer.loadDataFromArgument(alarm);

    Uri uri = RingtoneManager
        .getActualDefaultRingtoneUri(ApplicationComponentImpl.getInstance()
            .getApplicationComponent().provideContext(), RingtoneManager.TYPE_RINGTONE);

    String ringtone;
    if (uri == null) {
      ringtone = "";
    } else {
      ringtone = uri.toString();
    }

    Bundle bundle = new Bundle();
    bundle.putString(SET_RINGTONE_S_PARAM, ringtone);
    alarmFragmentReducer.reduceEvent(AlarmFragmentReducer.SET_RINGTONE_S, bundle);
  }

  @Override
  public void connect(final AlarmFragmentFragment alarmFragment) {

    alarmFragment.getLifecycle().addObserver(new DefaultLifecycleObserver() {
      @Override
      public void onStart(@NonNull LifecycleOwner owner) {
        alarmBuilderScreenRouter.setFragmentManager(alarmFragment.getFragmentManager());

      }

      @Override
      public void onStop(@NonNull LifecycleOwner owner) {
        alarmBuilderScreenRouter.clearFragmentManager();
      }
    });
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    FileLog.getInstance().v("LC", "AlarmViewModel onCleared");
  }

  @Override
  public void setVibration(boolean o) {
    Bundle bundle = new Bundle();
    bundle.putBoolean(SET_VIBRATION_B_PARAM, o);
    alarmFragmentReducer.reduceEvent(AlarmFragmentReducer.SET_VIBRATION_B, bundle);
  }

  @Override
  public void setRingtone(String path) {

    Bundle bundle = new Bundle();
    bundle.putString(SET_RINGTONE_S_PARAM, path);
    alarmFragmentReducer.reduceEvent(AlarmFragmentReducer.SET_RINGTONE_S, bundle);
  }

  @Override
  public void setDays(int oldDays) {
    Bundle bundle = new Bundle();
    bundle.putInt(SET_DAYS_I_PARAM, oldDays);
    alarmFragmentReducer.reduceEvent(AlarmFragmentReducer.SET_DAYS_I, bundle);
  }

  @Override
  public void showWeekDialog(@Nullable FragmentManager fragmentManager) {
  /*  alarmBuilderScreenRouter
        .showWeekDialog(currentState.alarmPOJO.getDays(), days -> currentState.setDays(days));*/
  }

  @Override
  public void showRingtoneDialog() {
    //  alarmFragmentView.showRingtoneDialog(currentState.alarmPOJO.getRingtone());
  }


  @Override
  public void showWorkDialog() {
    //  alarmFragmentView.showWorkDialog(currentState.alarmPOJO.getType());
  }

  @Override
  public void setType(int type) {

    String answer;

    if (type == MATH_TYPE) {
      answer = MathFragment.getMathEquation();
      //alarmTask.setAnswer(MathFragment.getMathEquation());
    } else {
      answer = "";
      //currentState.alarmPOJO.setAnswer("");
      //alarmTask.setAnswer("");
    }

    Bundle bundle = new Bundle();
    bundle.putInt(SET_TYPE_I_PARAM, type);
    bundle.putString(SET_ANSWER_S_PARAM, answer);
    alarmFragmentReducer.reduceEvent(AlarmFragmentReducer.SET_TYPE_I, bundle);
  }

  @Override
  public void saveAlarm(AlarmFragmentFragment alarmFragment, int minute, int hour) {

    //currentState.setHourAndMinute(hour, minute);

    Alarm alarm = alarmFragmentReducer.getAlarm();
    if (alarm == null) {
      return;
    }

    if (alarm.getId() != 0) {
      //appAlarmManager.cancelAlarm(alarm.getId());
    } else {

    }

    if (alarm.getId() == 0) {
      alarm.setId(System.currentTimeMillis());
    }
    if (alarm.getId() == 0) {
      alarm.setId(alarm.getId());
    }

    alarm.setState(Alarm.ALARM_ON);

    alarm.setMinute(minute);
    alarm.setHour(hour);

    Calendar launchCalendar = Calendar.getInstance();
    launchCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
    launchCalendar.setTimeInMillis(System.currentTimeMillis());

    int currentDays = launchCalendar.get(Calendar.DAY_OF_WEEK);
    int currentHour = launchCalendar.get(Calendar.HOUR_OF_DAY);
    int currentMinute = launchCalendar.get(Calendar.MINUTE);

      /*CreateAlarmReport report = appAlarmManager
          .setAlarm(alarm.getType(), currentDays, currentHour, currentMinute, alarm.getDays(),
              alarm.getHour(), alarm.getMinute(), alarm.getId(),
              launchCalendar.getTime().getTime());*/

    //long time = report.getTime();
    // alarm.setTime(time);

    //alarmView.showToast(time);
/*
    repoAlarmDao.addAlarmWithTask(alarm, alarmTask);*/
  }

  @Override
  public void popBack(FragmentActivity activity) {
    //AppFragmentManager.getInstance().onBackPressedNewVersion(activity);
  }

  // State sector

}
