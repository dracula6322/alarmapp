package square.green.com.fragmentmanager;

import android.os.Bundle;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.TwoStatePreference;


public class AlarmPreferenceFragment extends PreferenceFragmentCompat {

    private Preference repeat;
    private TwoStatePreference vibration;
    private Preference ringtone;
    private Preference work;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.alarm_preference, null);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        repeat = getPreferenceScreen().findPreference("repeat");
        ringtone = getPreferenceScreen().findPreference("ringtone");
        work = getPreferenceScreen().findPreference("work");
        vibration = (TwoStatePreference) getPreferenceScreen().findPreference("vibration");
    }

    public Preference getRepeat() {return repeat;}

    public Preference getRingtone() {
        return ringtone;
    }

    public Preference getWork() {
        return work;
    }

    public TwoStatePreference getVibration() {
        return vibration;
    }

}
