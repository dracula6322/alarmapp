package square.green.com.fragmentmanager.launchFragment;

import android.os.Bundle;

interface LaunchPresenter {

  void connect(LaunchFragment launchFragment, AlarmRecyclerViewAdapter adapter);

  void sendIntent(int value, Bundle bundle);
}
