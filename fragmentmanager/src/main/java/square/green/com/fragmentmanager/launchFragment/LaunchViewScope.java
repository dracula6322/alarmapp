package square.green.com.fragmentmanager.launchFragment;

import square.green.com.di.ApplicationComponentImpl;

class LaunchViewScope {

  private static volatile LaunchViewScope Instance;

  public static LaunchViewScope getInstance() {
    LaunchViewScope localInstance = Instance;
    if (localInstance == null) {
      synchronized (ApplicationComponentImpl.class) {
        localInstance = Instance;
        if (localInstance == null) {
          Instance = localInstance = new LaunchViewScope();
        }
      }
    }
    return localInstance;
  }


}
