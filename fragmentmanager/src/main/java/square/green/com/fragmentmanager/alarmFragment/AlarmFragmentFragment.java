package square.green.com.fragmentmanager.alarmFragment;

import static task.job.example.utils.TimeUtils.works;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TimePicker;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;
import androidx.preference.Preference;
import androidx.preference.TwoStatePreference;
import square.green.com.fragmentmanager.AlarmPreferenceFragment;
import square.green.com.fragmentmanager.AlarmPresenter;
import square.green.com.fragmentmanager.R;
import square.green.com.fragmentmanager.dialog.RingtoneBottomSheetDialogFragment;
import square.green.com.fragmentmanager.dialog.WorkBottomSheetDialogFragment;
import task.job.example.utils.TimeUtils;
import task.job.example.utils.model.Alarm;

public class AlarmFragmentFragment extends Fragment implements AlarmFragmentView {

  static final String ALARM_ARGUMENT = "ALARM_ARGUMENT";

  private Preference work;
  private Preference repeat;
  private Preference ringtone;
  private TwoStatePreference vibration;
  private TimePicker timePicker;
  private AlarmPresenter alarmPresenter;

  @NonNull
  public static Bundle generateBundle(@Nullable Alarm alarm) {

    Bundle bundle = new Bundle();
    bundle.putParcelable(ALARM_ARGUMENT, alarm);
    return bundle;
  }

  public static AlarmFragmentFragment newInstance(@Nullable Alarm alarm) {

    Bundle bundle = generateBundle(alarm);
    AlarmFragmentFragment alarmFragment = new AlarmFragmentFragment();
    alarmFragment.setArguments(bundle);
    return alarmFragment;

  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    alarmPresenter = AlarmViewModel.buildAlarmViewModel(this, getArguments());
    alarmPresenter.connect(this);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.alarm_fragment, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    AlarmPreferenceFragment preferenceFragment = (AlarmPreferenceFragment) getChildFragmentManager()
        .findFragmentByTag("alarm_preference");
    initPreferenceFragment(preferenceFragment);

    timePicker = view.findViewById(R.id.alarm_fragment_time_picker);
    timePicker.setIs24HourView(true);

    view.findViewById(R.id.alarm_fragment_agree).setOnClickListener(view1 -> {

      int hour;
      int minute;
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        hour = timePicker.getHour();
        minute = timePicker.getMinute();
      } else {
        hour = timePicker.getCurrentHour();
        minute = timePicker.getCurrentMinute();
      }
      alarmPresenter.saveAlarm(this, minute, hour);
      alarmPresenter.popBack(getActivity());
    });

    view.findViewById(R.id.alarm_fragment_cancel)
        .setOnClickListener(v -> alarmPresenter.popBack(getActivity()));
  }

  private void initPreferenceFragment(AlarmPreferenceFragment preferenceFragment) {

    if (getActivity() == null || preferenceFragment == null) {
      return;
    }

    repeat = preferenceFragment.getRepeat();
    repeat.setIcon(AppCompatResources.getDrawable(getActivity(), R.drawable.baseline_replay));
    repeat.setOnPreferenceClickListener(preference -> {
      alarmPresenter.showWeekDialog(getFragmentManager());
      return true;
    });

    vibration = preferenceFragment.getVibration();
    vibration.setIcon(AppCompatResources.getDrawable(getActivity(), R.drawable.vibration_vector));
    vibration.setOnPreferenceChangeListener((preference, o) -> {
      alarmPresenter.setVibration((boolean) o);
      return true;
    });

    work = preferenceFragment.getWork();
    work.setIcon(AppCompatResources.getDrawable(getActivity(), R.drawable.baseline_unlock));
    work.setOnPreferenceClickListener(preference -> {
      alarmPresenter.showWorkDialog();
      return true;
    });

    ringtone = preferenceFragment.getRingtone();
    ringtone.setIcon(
        AppCompatResources.getDrawable(getActivity(), R.drawable.baseline_music_note_white_24));
    ringtone.setOnPreferenceClickListener(preference -> {
      alarmPresenter.showRingtoneDialog();
      return true;
    });

  }

  @Override
  public void showWorkDialog(int type) {

    if (getFragmentManager() == null) {
      return;
    }

    WorkBottomSheetDialogFragment fragment = WorkBottomSheetDialogFragment
        .newInstance(alarmPresenter, type);
    fragment.show(getFragmentManager(), "showWorkDialog");
  }

  @Override
  public void showRingtoneDialog(String ringtone) {

    if (getFragmentManager() == null) {
      return;
    }

    RingtoneBottomSheetDialogFragment ringtoneBottomSheetDialogFragment = RingtoneBottomSheetDialogFragment
        .newInstance(ringtone, null);
    ringtoneBottomSheetDialogFragment.show(getFragmentManager(), "showRingtoneDialog");
  }

  @Override
  public void showWeekDialog(final int days) {
    alarmPresenter.showWeekDialog(getFragmentManager());
  }


  @Override
  public void drawView(int minute, int hour, int days, String ringtone, boolean vibrate, int type) {

    if (hour != -1 && minute != -1) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        timePicker.setHour(hour);
        timePicker.setMinute(minute);
      } else {
        timePicker.setCurrentHour(hour);
        timePicker.setCurrentMinute(minute);
      }
    }

    drawType(type);
    drawAlarmDays(days);
    drawRingtoneSetting(ringtone);
    vibration.setChecked(vibrate);
  }

  @Override
  public void showToast(long time) {

    Toast.makeText(getContext(),
        "Будильник сработает через " + TimeUtils.stringTimeToAlarm(time, false),
        Toast.LENGTH_LONG).show();
  }

  private void drawType(int type) {
    work.setSummary(works[type]);
  }

  private void drawRingtoneSetting(String path) {

    if (path == null || path.isEmpty()) {
      this.ringtone.setSummary(R.string.silent);
      this.ringtone.setIcon(
          AppCompatResources.getDrawable(getActivity(), R.drawable.baseline_music_off_white_24));
    } else {

      Ringtone currentRingtone = RingtoneManager.getRingtone(getContext(), Uri.parse(path));

      this.ringtone.setSummary(currentRingtone.getTitle(getActivity()));
      this.ringtone.setIcon(
          AppCompatResources.getDrawable(getActivity(), R.drawable.baseline_music_note_white_24));
    }
  }

  private void drawAlarmDays(int days) {
    repeat.setSummary(TimeUtils.getWeekString(getResources(), days));
  }

  @Override
  public Fragment getThis() {
    return this;
  }
}
