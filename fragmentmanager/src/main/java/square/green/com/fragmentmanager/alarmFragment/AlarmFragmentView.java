package square.green.com.fragmentmanager.alarmFragment;

import androidx.fragment.app.Fragment;

public interface AlarmFragmentView {


  void showToast(long time);

  Fragment getThis();

  void showWorkDialog(int type);

  void showRingtoneDialog(String ringtone);

  void showWeekDialog(final int days);

  void drawView(int minute, int hour, int days, String ringtone, boolean vibrate, int type);

  class Stub implements AlarmFragmentView {

    @Override
    public Fragment getThis() {
      return null;
    }

    @Override
    public void showRingtoneDialog(String ringtone) {

    }

    @Override
    public void showWeekDialog(int days) {

    }

    @Override
    public void showWorkDialog(int type) {

    }

    @Override
    public void drawView(int minute, int hour, int days, String ringtone, boolean vibrate,
        int type) {

    }

    @Override
    public void showToast(long time) {

    }
  }

}
