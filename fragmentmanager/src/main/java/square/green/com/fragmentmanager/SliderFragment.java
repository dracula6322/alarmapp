package square.green.com.fragmentmanager;

import static task.job.example.utils.api.AlarmWorkApi.ALARM_ID_L;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import task.job.example.utils.SliderView;


public class SliderFragment extends AlarmTaskFragment implements SliderView.OnSlideCompleteListener{

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    long idAlarm = getArguments().getLong(ALARM_ID_L, -1);
    setIdAlarm(idAlarm);

  }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sliderfragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SliderView slider = view.findViewById(R.id.alarm_slider_fragment);
        slider.setText("Slide to unlock");
        slider.setOnSlideCompleteListener(this);
    }

    @Override
    public void onSlideComplete() {
        switchOffAlarm();
    }
}
