package square.green.com.fragmentmanager;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

public class DayTextView extends AppCompatTextView {

    private boolean chosen;
    private Paint paint;

    public DayTextView(Context context) {
        super(context);
        init();
    }

    public DayTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DayTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(2);
        paint.setStyle(Paint.Style.STROKE);

        setChosen(false);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (isChosen()) {
            int x = getHeight() / 2;
            int y = getWidth() / 2;
            canvas.drawCircle(y, x, x * 4 / 9, paint);
        }
    }

    @Override
    public boolean performClick() {
        setChosen(!isChosen());
        return super.performClick();
    }

    private boolean isChosen() {
        return chosen;
    }

    public void setChosen(boolean chosen) {
        this.chosen = chosen;
    }
}
