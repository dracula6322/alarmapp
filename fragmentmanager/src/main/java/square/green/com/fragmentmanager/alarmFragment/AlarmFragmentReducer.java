package square.green.com.fragmentmanager.alarmFragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import square.green.com.fragmentmanager.alarmFragment.AlarmFragmentView.Stub;
import task.job.example.utils.model.Alarm;
import task.job.example.utils.model.AlarmBuilder;

public class AlarmFragmentReducer {

  public static final int SET_VIBRATION_B = 1;
  public static final String SET_VIBRATION_B_PARAM = "SET_VIBRATION_B";

  public static final int SET_RINGTONE_S = 2;
  public static final String SET_RINGTONE_S_PARAM = "SET_RINGTONE_S";

  public static final int SET_DAYS_I = 3;
  public static final String SET_DAYS_I_PARAM = "SET_DAYS_I";


  public static final int SET_TYPE_I = 4;
  public static final String SET_TYPE_I_PARAM = "SET_TYPE_I";
  public static final String SET_ANSWER_S_PARAM = "SET_ANSWER_S_PARAM";

  public State currentState = new EmptyState();
  public AlarmFragmentView alarmFragmentView = new Stub();

  private DefaultLifecycleObserver defaultLifecycleObserver = new DefaultLifecycleObserver() {
    @Override
    public void onStart(@NonNull LifecycleOwner owner) {
      currentState.drawState();
    }

    @Override
    public void onStop(@NonNull LifecycleOwner owner) {
      owner.getLifecycle().removeObserver(this);
      setLaunchView(new Stub());
    }
  };

  public void setLaunchView(@Nullable AlarmFragmentView alarmFragmentView) {

    if (alarmFragmentView == null) {
      this.alarmFragmentView = new Stub();
    } else {
      this.alarmFragmentView = alarmFragmentView;
    }
  }

  public void addLaunchView(Lifecycle lifecycle) {
    lifecycle.addObserver(defaultLifecycleObserver);
  }

  public Object reduceEvent(int value, @NonNull Bundle bundle) {

    if (value == SET_VIBRATION_B) {

      currentState.setVibrate(bundle.getBoolean(SET_VIBRATION_B_PARAM));
      return null;
    }

    if (value == SET_RINGTONE_S) {
      currentState.setRingtone(bundle.getString(SET_RINGTONE_S_PARAM));
      return null;
    }

    if (value == SET_DAYS_I) {
      currentState.setDays(bundle.getInt(SET_DAYS_I_PARAM));
      return null;
    }

    if (value == SET_TYPE_I) {
      currentState.setType(bundle.getInt(SET_TYPE_I_PARAM), bundle.getString(SET_ANSWER_S_PARAM));
      return null;
    }

    return null;
  }

  public void loadDataFromArgument(Alarm alarm) {
    currentState.loadDataFromArgument(alarm);
  }

  public Alarm getAlarm() {
    return currentState.buildAlarm();
  }


  public abstract class State {

    public void loadDataFromArgument(Alarm alarm) {
    }

    public void drawState() {
    }

    public void setRingtoneAndAlarm(String ringtone, boolean vibrate) {
    }

    public void setVibrate(boolean isVibrate) {
    }

    public void setRingtone(String path) {
    }

    public void setDays(int oldDays) {
    }

    public void setType(int type, String answer) {
    }

    public void setHourAndMinute(int hour, int minute) {
    }

    public Alarm buildAlarm() {
      return null;
    }

  }

  public class EmptyState extends State {

    public void loadDataFromArgument(@NonNull Alarm alarm) {
      currentState = new AlarmState(alarm);
    }
  }

  public class AlarmState extends State {

    private final AlarmBuilder alarmBuilder;

    AlarmState(@NonNull Alarm alarm) {
      this.alarmBuilder = AlarmBuilder.anAlarm(alarm);
      drawState();
    }

    @Override
    public void setVibrate(boolean isVibrate) {
      alarmBuilder.withIsVibrate(isVibrate);
      drawState();
    }

    @Override
    public void setRingtone(String path) {
      alarmBuilder.withRingtone(path);
      drawState();
    }

    @Override
    public void setDays(int oldDays) {
      alarmBuilder.withDays(oldDays);
      drawState();
    }

    @Override
    public void setType(int type, String answer) {
      alarmBuilder.withAnswerType(type);
      alarmBuilder.withAnswer(answer);
      drawState();
    }

    @Override
    public void setHourAndMinute(int hour, int minute) {
      alarmBuilder.withHour(hour);
      alarmBuilder.withMinute(minute);
    }

    @Override
    public void drawState() {
      alarmFragmentView
          .drawView(alarmBuilder.getMinute(), alarmBuilder.getHour(), alarmBuilder.getDays(),
              alarmBuilder.getRingtone(), alarmBuilder.isVibrate(), alarmBuilder.getType());
    }

    @Override
    public Alarm buildAlarm() {
      return alarmBuilder.build();
    }
  }

}
