package square.green.com.fragmentmanager.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import java.util.List;
import square.green.com.fragmentmanager.AlarmRingtoneManager;
import square.green.com.fragmentmanager.R;

public class RingtoneBottomSheetDialogFragment extends BottomSheetDialogFragment {

  private static final String TAG_RINGTONE_S = "TAG_RINGTONE_S";

  private Ringtone tmp;
  @Nullable
  private String ringtone;

  private final Consumer<String> emptyConsumer = integer -> {
  };
  private Consumer<String> ringtoneConsumer = emptyConsumer;

  public static RingtoneBottomSheetDialogFragment newInstance(@Nullable String ringtone,
      @NonNull Consumer<String> ringtoneConsumer) {
    Log.v("TAG", "putInArgument " + ringtone);
    RingtoneBottomSheetDialogFragment weekBottomSheetDialogFragment = new RingtoneBottomSheetDialogFragment();
    weekBottomSheetDialogFragment.setRingtoneConsumer(ringtoneConsumer);
    Bundle bundle = new Bundle();
    bundle.putString(TAG_RINGTONE_S, ringtone);
    weekBottomSheetDialogFragment.setArguments(bundle);
    return weekBottomSheetDialogFragment;
  }

  private void setRingtoneConsumer(Consumer<String> ringtoneConsumer) {
    this.ringtoneConsumer = ringtoneConsumer;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.layout_list_view_ringtone_bsdf, container, false);
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    ringtone = requireArguments().getString(TAG_RINGTONE_S, null);
    Log.v("TAG", "getFromArgument " + ringtone);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    ListView ringtoneListView = view.findViewById(R.id.lvMain);
    ringtoneListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

    AlarmRingtoneManager alarmRingtoneManager = new AlarmRingtoneManager();
    RingtoneManagerReport ringtoneManagerReport = alarmRingtoneManager
        .getAllSystemRingtone(getContext(), getString(R.string.silent));

    if (ringtoneManagerReport.isEmpty()) {
      Toast.makeText(getContext(), getString(R.string.no_system_ringtones), Toast.LENGTH_LONG)
          .show();
      dismiss();
      return;
    }

    Context context = getContext();
    if (context == null) {
      return;
    }

    ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(context,
        android.R.layout.simple_list_item_single_choice, ringtoneManagerReport.ringtoneName);
    ringtoneListView.setAdapter(adapter);

    Log.v("TAG", "getFromArgument " + ringtone);
    if (ringtone == null) {
      Uri defaultRingtone = alarmRingtoneManager.getDefaultRingtone(getContext());
      ringtone = defaultRingtone.toString();
    }

    int findCount = ringtoneManagerReport.paths.indexOf(ringtone);
    Log.v("TAG", "findCount " + findCount);
    ringtoneListView.setItemChecked(findCount, true);

    int position = findCount;
    if (position < 2) {
      position = 0;
    } else if (position > (adapter.getCount() - 2)) {
      position = adapter.getCount();
    } else {
      position -= 2;
    }

    ringtoneListView.setSelection(position);

    ringtoneListView.setOnItemClickListener((parent, view1, position1, id) -> {
      if (tmp != null) {
        tmp.stop();
      }

      String path = ringtoneManagerReport.paths.get(position1);
      ringtoneConsumer.accept(path);

      if (path == null || path.isEmpty()) {
        return;
      }

      tmp = RingtoneManager.getRingtone(getContext(), Uri.parse(path));
      tmp.play();
    });
  }

  @Override
  public void onCancel(@NonNull DialogInterface dialog) {
    super.onCancel(dialog);
    clearAll();
  }

  @Override
  public void onDismiss(@NonNull DialogInterface dialog) {
    super.onDismiss(dialog);
    clearAll();
  }

  private void clearAll() {
    if (tmp != null) {
      tmp.stop();
    }
    ringtoneConsumer = emptyConsumer;
  }

  static public class RingtoneManagerReport {

    final List<String> paths;
    final List<CharSequence> ringtoneName;


    public RingtoneManagerReport(@NonNull List<CharSequence> names, @NonNull List<String> paths) {
      this.ringtoneName = names;
      this.paths = paths;
    }

    public boolean isEmpty() {
      return ringtoneName.isEmpty();
    }

  }

}