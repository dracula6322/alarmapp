package square.green.com.fragmentmanager.launchFragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import square.green.com.fragmentmanager.AlarmTaskFragment;
import square.green.com.fragmentmanager.AppFragmentManager;
import task.job.example.utils.model.Alarm;

class LaunchRouter {

  private FragmentManager fragmentManager;

  public void showAlarmTaskFragment(@Nullable Alarm alarm) {

    if (fragmentManager == null) {
      return;
    }

    Bundle bundle = AlarmTaskFragment.generateBundle(alarm);

    AppFragmentManager.getInstance()
        .showAlarmTaskFragment(fragmentManager, AppFragmentManager.idFragment,
            alarm.getAnswerType(),
            bundle);
  }

  public void setFragmentManager(FragmentManager fragmentManager) {
    this.fragmentManager = fragmentManager;
  }

  public void clearFragmentManager() {
    this.fragmentManager = null;
  }

}
