package square.green.com.fragmentmanager.alarmFragment;

import androidx.core.util.Consumer;
import androidx.fragment.app.FragmentManager;
import square.green.com.fragmentmanager.dialog.WeekBottomSheetDialogFragment;

class AlarmBuilderScreenRouter {

  private FragmentManager fragmentManager;

  public void setFragmentManager(FragmentManager fragmentManager) {
    this.fragmentManager = fragmentManager;
  }

  public void clearFragmentManager() {
    this.fragmentManager = null;
  }

  public void showWeekDialog(int currentDays, Consumer<Integer> consumer) {

    if (fragmentManager == null) {
      return;
    }

    WeekBottomSheetDialogFragment addPhotoBottomDialogFragment = WeekBottomSheetDialogFragment
        .newInstance(currentDays, consumer);

    addPhotoBottomDialogFragment.show(fragmentManager, "showWeekDialog");

  }
}
