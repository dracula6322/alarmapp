package square.green.com.fragmentmanager;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import square.green.com.fragmentmanager.alarmFragment.AlarmFragmentFragment;

public interface AlarmPresenter {

  void showRingtoneDialog();

  void saveAlarm(AlarmFragmentFragment alarmFragment, int minute, int hour);

  void popBack(FragmentActivity activity);

  void setRingtone(String path);

  void setDays(int oldDays);

  void showWeekDialog(FragmentManager fragmentManager);

  void connect(AlarmFragmentFragment alarmFragment);

  void setVibration(boolean o);

  void showWorkDialog();

  void setType(int sliderType);
}
