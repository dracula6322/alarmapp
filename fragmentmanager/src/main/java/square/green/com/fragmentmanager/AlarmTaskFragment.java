package square.green.com.fragmentmanager;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import square.green.com.di.ApplicationComponentImpl;
import task.job.example.utils.api.AlarmWorkLiveApi;
import task.job.example.utils.model.Alarm;

abstract public class AlarmTaskFragment extends Fragment implements SwitchOffAlarm {

  private static final String ALARM_ID_L = "ALARM_ID_L";
  private long idAlarm;

  private AlarmWorkLiveApi alarmWorkApi = ApplicationComponentImpl.getInstance()
      .getApplicationComponent().provideAlarmWorkLiveApi();


  @NonNull
  public static Bundle generateBundle(Alarm alarm) {

    Bundle bundle = new Bundle();
    bundle.putLong(ALARM_ID_L, alarm.getId());
    return bundle;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    if (getArguments() != null) {
      setIdAlarm(getArguments().getLong(ALARM_ID_L));
    }


  }

  public void setIdAlarm(long idAlarm) {
    this.idAlarm = idAlarm;
  }

  @Override
  public void switchOffAlarm() {

    if (getActivity() == null) {
      return;
    }

    alarmWorkApi.stopAlarmService(getActivity());

    AppFragmentManager.getInstance().closeAlarm(getActivity());
  }
}
