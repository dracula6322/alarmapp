package square.green.com.fragmentmanager.dialog;

import static task.job.example.utils.TimeUtils.MATH_TYPE;
import static task.job.example.utils.TimeUtils.NOTIFICATION_TYPE;
import static task.job.example.utils.TimeUtils.SLIDER_TYPE;
import static task.job.example.utils.TimeUtils.works;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import square.green.com.fragmentmanager.AlarmPresenter;
import square.green.com.fragmentmanager.R;

public class WorkBottomSheetDialogFragment extends BottomSheetDialogFragment {

    private static String TAG_WORK_I = "TAG_WORK_I";

    private AlarmPresenter alarmPresenter;

    public static WorkBottomSheetDialogFragment newInstance(AlarmPresenter alarmPresenter, int currentWork) {
        Bundle bundle = new Bundle();
        bundle.putInt(TAG_WORK_I, currentWork);
        WorkBottomSheetDialogFragment weekBottomSheetDialogFragment = new WorkBottomSheetDialogFragment();
        weekBottomSheetDialogFragment.setAlarmPresenter(alarmPresenter);
        weekBottomSheetDialogFragment.setArguments(bundle);

        return weekBottomSheetDialogFragment;
    }

    private void setAlarmPresenter(AlarmPresenter alarmPresenter) {
        this.alarmPresenter = alarmPresenter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_list_view_bsdf, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListView lvMain = view.findViewById(R.id.lvMain);
        lvMain.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_single_choice, works);

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    alarmPresenter.setType(SLIDER_TYPE);
                }
                if (position == 1) {
                    alarmPresenter.setType(NOTIFICATION_TYPE);
                }
                if (position == 2) {
                    alarmPresenter.setType(MATH_TYPE);
                }
            }
        });

        lvMain.setAdapter(adapter);
        lvMain.setItemChecked(getArguments().getInt(TAG_WORK_I), true);
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);


    }
}
