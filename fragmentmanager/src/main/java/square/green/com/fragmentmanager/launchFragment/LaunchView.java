package square.green.com.fragmentmanager.launchFragment;

import androidx.annotation.NonNull;
import java.util.List;
import task.job.example.utils.model.Alarm;

interface LaunchView {

  class Stub implements LaunchView {

    @Override
    public boolean updateTimeInHeaderRecyclerView() {
      return false;
    }

    @Override
    public void drawView(boolean loadingProgress, @NonNull List<Alarm> data) {

    }
  }

  boolean updateTimeInHeaderRecyclerView();

  void drawView(boolean loadingProgress, @NonNull List<Alarm> data);
}
