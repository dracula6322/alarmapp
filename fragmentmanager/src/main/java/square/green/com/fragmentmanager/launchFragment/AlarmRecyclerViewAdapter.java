package square.green.com.fragmentmanager.launchFragment;

import static task.job.example.utils.TimeUtils.MATH_TYPE;
import static task.job.example.utils.TimeUtils.NOTIFICATION_TYPE;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import square.green.com.di.ApplicationComponentImpl;
import square.green.com.fragmentmanager.R;
import square.green.com.fragmentmanager.alarmFragment.AlarmFragmentFragment;
import task.job.example.utils.TimeUtils;
import task.job.example.utils.model.Alarm;

class AlarmRecyclerViewAdapter extends RecyclerView.Adapter<AlarmRecyclerViewAdapter.MyHolder> {

  private final LaunchPresenter launchPresenter;
  private List<Alarm> alarms = new ArrayList<>();
  private long workingAlarmId = -1;
  private AnimatorSet set = new AnimatorSet();

  public AlarmRecyclerViewAdapter(LaunchPresenter launchPresenter) {
    this.launchPresenter = launchPresenter;
  }

  @NonNull
  @Override
  public MyHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
    View view = LayoutInflater
        .from(ApplicationComponentImpl.getInstance().getApplicationComponent().provideContext())
        .inflate(R.layout.alarm_recycler_view, parent, false);
    return new MyHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull final MyHolder holder, final int position) {

    final Alarm alarm = alarms.get(holder.getAdapterPosition());

    setDrawable(alarm, holder.alarmTask);

    String hour = String.valueOf(alarm.getHour());
    if (hour.length() == 1) {
      hour = '0' + hour;
    }
    String minute = String.valueOf(alarm.getMinute());
    if (minute.length() == 1) {
      minute = '0' + minute;
    }
    StringBuilder stringTime = new StringBuilder(hour).append(':').append(minute);
    holder.time.setText(stringTime);

    holder.week
        .setText(TimeUtils.getWeekString(
            ApplicationComponentImpl.getInstance().getApplicationComponent().provideContext()
                .getResources(), alarm.getDays()));

    if (alarm.getState() == Alarm.ALARM_ON || alarm.getId() == workingAlarmId) {
      holder.enableSwitch.setChecked(true);
    } else {
      holder.enableSwitch.setChecked(false);
    }

    if (alarm.getId() == workingAlarmId) {
      holder.threeDots.setClickable(false);
    } else {
      holder.threeDots.setClickable(true);
    }

    if (alarm.getId() == workingAlarmId) {
      holder.note.setVisibility(View.VISIBLE);
      holder.note.setText(
          ApplicationComponentImpl.getInstance().getApplicationComponent().provideContext()
              .getString(R.string.tap_to_turn_off));
    } else if (alarm.getState() == Alarm.ALARM_ON) {
      String time = TimeUtils.stringTimeToAlarm(alarm.getTime(), true);
      holder.note.setVisibility(View.VISIBLE);
      holder.note.setText(time);
    } else if (alarm.getState() == Alarm.ALARM_OFF) {
      holder.note.setVisibility(View.GONE);
    }

    if (alarm.getId() == workingAlarmId) {
      holder.showActivity.setVisibility(View.VISIBLE);
      holder.enableSwitch.setVisibility(View.GONE);
    } else {
      holder.showActivity.setVisibility(View.GONE);
      holder.enableSwitch.setVisibility(View.VISIBLE);
    }


    if (alarm.getId() == workingAlarmId) {
      startAnimation(holder.showActivity);
    }

    holder.time.setTextColor(
        alarm.getState() == Alarm.ALARM_ON ? Color.WHITE : Color.parseColor("#bdbdbd"));
    holder.note.setTextColor(
        alarm.getState() == Alarm.ALARM_ON ? Color.WHITE : Color.parseColor("#bdbdbd"));
    holder.week.setTextColor(
        alarm.getState() == Alarm.ALARM_ON ? Color.WHITE : Color.parseColor("#bdbdbd"));

    holder.alarmView.setOnClickListener(v -> {
      if (alarm.getId() == workingAlarmId) {

        Bundle bundle = AlarmFragmentFragment.generateBundle(alarm);

        launchPresenter
            .sendIntent(LaunchViewModel.LAUNCH_OPEN_ALARM_FRAGMENT_INTENT, bundle);
      } else {
        launchPresenter
            .sendIntent(LaunchViewModel.DELETE_ALARM, new Bundle());
      }
    });

    holder.threeDots.setOnClickListener(v -> {

      PopupMenu popupMenu = new PopupMenu(v.getContext(), v);
      popupMenu.getMenu().add(0, 0, 0,
          ApplicationComponentImpl.getInstance().getApplicationComponent().provideContext()
              .getString(R.string.delete));
      popupMenu.show();
      popupMenu.setOnMenuItemClickListener(item -> {
        launchPresenter
            .sendIntent(LaunchViewModel.DELETE_ALARM, new Bundle());

        return false;
      });
    });

    holder.enableSwitchLayout.setOnClickListener(v -> {

      if (alarm.getId() != workingAlarmId) {
        holder.enableSwitch.performClick();
      } else {

        Bundle bundle = new Bundle();
        bundle.putParcelable("ALARM", alarm);

        launchPresenter
            .sendIntent(LaunchViewModel.LAUNCH_OPEN_ALARM_FRAGMENT_INTENT, bundle);
        return;
      }

      boolean state = holder.enableSwitch.isChecked();
      if (state) {
        launchPresenter
            .sendIntent(LaunchViewModel.ON_ALARM, new Bundle());
      } else {
        launchPresenter
            .sendIntent(LaunchViewModel.OFF_ALARM, new Bundle());
      }
    });
  }

  @Override
  public int getItemCount() {
    return alarms != null ? alarms.size() : 0;
  }

  void setAlarms(List<Alarm> alarms) {
    this.alarms = alarms;
    Collections.sort(alarms, (o1, o2) -> Long.compare(o1.getTime(), o2.getTime()));
    notifyDataSetChanged();
  }

  Observer<Long> currentAlarmWorkId = id -> {
    workingAlarmId = (id != null ? id : -1);
    notifyDataSetChanged();
  };

  public void startObserve(LifecycleOwner owner, MutableLiveData<Long> liveDataWorking) {

    owner.getLifecycle().addObserver(new DefaultLifecycleObserver() {
      @Override
      public void onStart(@NonNull LifecycleOwner owner) {
        liveDataWorking.observeForever(currentAlarmWorkId);
      }

      @Override
      public void onStop(@NonNull LifecycleOwner owner) {
        liveDataWorking.removeObserver(currentAlarmWorkId);
        stopAnimation();
      }
    });
  }

  class MyHolder extends RecyclerView.ViewHolder {

    final ImageView alarmTask;
    final View alarmView;
    final TextView time;
    final TextView note;
    final TextView week;
    final View threeDots;
    final CheckBox enableSwitch;
    final ViewGroup enableSwitchLayout;
    final View showActivity;

    MyHolder(View itemView) {
      super(itemView);
      note = itemView.findViewById(R.id.alarm_note_recycler_view);
      alarmTask = itemView.findViewById(R.id.alarm_task_recycler_view);
      alarmView = itemView.findViewById(R.id.alarm_id_recycler_view);
      time = itemView.findViewById(R.id.alarm_time_recycler_view);
      enableSwitch = itemView.findViewById(R.id.alarm_enable_switch_recycle_view);
      week = itemView.findViewById(R.id.alarm_week_recycler_view);
      threeDots = itemView.findViewById(R.id.alarm_three_dots_recycler_view);
      enableSwitchLayout = itemView.findViewById(R.id.alarm_enable_layout_switch_recycle_view);
      showActivity = itemView.findViewById(R.id.alarm_show_activity_recycler_view);
    }
  }

  private void setDrawable(Alarm alarmForList, ImageView imageView) {

    int descImage = R.drawable.baseline_swap_horiz_24;
    if (alarmForList.getAnswerType() == MATH_TYPE) {
      descImage = R.drawable.baseline_exposure_24;
    }
    if (alarmForList.getAnswerType() == NOTIFICATION_TYPE) {
      descImage = R.drawable.baseline_notification;
    }

    Drawable drawable = AppCompatResources.getDrawable(
        ApplicationComponentImpl.getInstance().getApplicationComponent().provideContext(),
        descImage);
    if (drawable == null) {
      return;
    }

    drawable = DrawableCompat.wrap(drawable).mutate();
    if (alarmForList.getState() == Alarm.ALARM_OFF) {
      DrawableCompat.setTint(drawable, Color.parseColor("#bdbdbd"));
    } else {
      DrawableCompat.setTint(drawable, Color.WHITE);
    }
    imageView.setImageDrawable(drawable);

  }

  private void startAnimation(View v) {
    if (set.isRunning() || set.isStarted()) {
      return;
    }


    ObjectAnimator animator1 = ObjectAnimator.ofFloat(v, "translationX", -10f);
    animator1.setDuration(600);

    ObjectAnimator animator2 = ObjectAnimator.ofFloat(v, "translationX", 10f);
    animator2.setDuration(600);

    ObjectAnimator animator3 = ObjectAnimator.ofFloat(v, "translationX", 0f);
    animator3.setDuration(600);

    set = new AnimatorSet();
    set.addListener(new AnimatorListenerAdapter() {
      private boolean mCanceled;

      @Override
      public void onAnimationStart(Animator animation) {
        mCanceled = false;
      }

      @Override
      public void onAnimationCancel(Animator animation) {
        mCanceled = true;
      }

      @Override
      public void onAnimationEnd(Animator animation) {
        if (!mCanceled) {
          animation.start();
        }
      }
    });
    set.play(animator1).before(animator2);
    set.play(animator2).before(animator3);
    set.start();

  }

  void stopAnimation() {
    set.cancel();
    set.removeAllListeners();
  }
}
