package square.green.com.fragmentmanager.launchFragment;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.List;
import square.green.com.fragmentmanager.R;
import task.job.example.utils.model.Alarm;

public class LaunchFragment extends Fragment implements LaunchView {

  public FloatingActionButton fab;
  private RecyclerView recyclerView;
  private View emptyView;
  private View progressBar;

  private AlarmRecyclerViewAdapter adapter;
  public LaunchPresenter launchViewModel;

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    Class<? extends ViewModel> viewModelClass = LaunchViewModel.class;

    launchViewModel = LaunchViewModel.buildLaunchViewModel(requireActivity(), viewModelClass);
    adapter = new AlarmRecyclerViewAdapter(launchViewModel);
    launchViewModel.connect(this, adapter);
  }


  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.launch_fragment, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    emptyView = view.findViewById(R.id.empty_layout_alarm_launch_fragment);
    progressBar = view.findViewById(R.id.progressBar);

    fab = view.findViewById(R.id.fab_launch_fragment);
    fab.setOnClickListener((v) -> launchViewModel
        .sendIntent(LaunchViewModel.LAUNCH_OPEN_ALARM_FRAGMENT_INTENT, new Bundle())
    );

    Drawable addBell = AppCompatResources
        .getDrawable(requireActivity(), R.drawable.baseline_add_alarm);
    fab.setImageDrawable(addBell);

    recyclerView = view.findViewById(R.id.alarm_recycler_view_fragment);

    recyclerView.setHasFixedSize(true);
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        if (newState == RecyclerView.SCROLL_STATE_IDLE) {

          int offset = recyclerView.computeVerticalScrollOffset();

          if (offset == 0 && !fab.isShown()) {
            fab.show();
          }

          if (offset != 0 && fab.isShown()) {
            fab.hide();
          }

        }
      }
    });
    Context context = getContext();
    if (context != null) {
      RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(context,
          DividerItemDecoration.VERTICAL) {
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
            RecyclerView.State state) {
          super.getItemOffsets(outRect, view, parent, state);
          outRect.top = 5;
          outRect.bottom = 5;
        }
      };
      recyclerView.addItemDecoration(itemDecoration);
    }
    recyclerView.setLayoutManager(new LinearLayoutManager(context));
    recyclerView.setAdapter(adapter);


  }

  @Override
  public void onStart() {
    super.onStart();
    recyclerView.setAdapter(adapter);
  }

  @Override
  public boolean updateTimeInHeaderRecyclerView() {
    FragmentActivity activity = getActivity();

    if (activity == null) {
      return false;
    }

    runInUiThread(() -> {
      if (adapter != null) {
        adapter.notifyDataSetChanged();
      }
    });
    return true;
  }

  @Override
  public void drawView(boolean loadingProgress, @NonNull List<Alarm> data) {

    runInUiThread(() -> {
      if (loadingProgress) {
        progressBar.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
      } else {
        progressBar.setVisibility(View.GONE);
      }

      if (data.isEmpty()) {
        emptyView.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
      } else {
        emptyView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
      }

      adapter.setAlarms(data);
      fab.show();
    });
  }

  private void runInUiThread(@NonNull Runnable runnable) {
    FragmentActivity activity = getActivity();

    if (activity == null) {
      return;
    }

    activity.runOnUiThread(runnable);
  }

}
