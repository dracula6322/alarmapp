package square.green.com.fragmentmanager.launchFragment;

import static square.green.com.fragmentmanager.launchFragment.LaunchViewModel.DELETE_ALARM;
import static square.green.com.fragmentmanager.launchFragment.LaunchViewModel.LAUNCH_OPEN_ALARM_FRAGMENT_INTENT;
import static square.green.com.fragmentmanager.launchFragment.LaunchViewModel.OFF_ALARM;
import static square.green.com.fragmentmanager.launchFragment.LaunchViewModel.ON_ALARM;
import static task.job.example.utils.api.AlarmWorkApi.ALARM_ID_L;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import java.util.Collections;
import java.util.List;
import square.green.com.di.ApplicationComponentImpl;
import square.green.com.fragmentmanager.launchFragment.LaunchView.Stub;
import square.green.com.utils.api.alarmrepo.AsyncAlarmRepoApi;
import task.job.example.utils.api.AlarmWorkApi;
import task.job.example.utils.model.Alarm;
import task.job.example.utils.response.CreateAlarmReport;

public class LaunchFragmentReducer {

  AsyncAlarmRepoApi asyncAlarmRepoApi = ApplicationComponentImpl.getInstance()
      .getApplicationComponent().provideAlarmRepoApi();

  private AlarmWorkApi alarmWorkApi = ApplicationComponentImpl.getInstance()
      .getApplicationComponent().provideAlarmWorker();

  @SuppressWarnings("WeakerAccess")
  LaunchFragmentState currentState = new EmptyState();
  private LaunchView launchView = new Stub();

  private DefaultLifecycleObserver defaultLifecycleObserver = new DefaultLifecycleObserver() {
    @Override
    public void onStart(@NonNull LifecycleOwner owner) {
      currentState.loadData();
    }

    @Override
    public void onStop(@NonNull LifecycleOwner owner) {
      owner.getLifecycle().removeObserver(this);
      setLaunchView(new Stub());
    }
  };


  public void addLaunchView(Lifecycle lifecycle) {
    lifecycle.addObserver(defaultLifecycleObserver);
  }

  public Object reduceEvent(int value) {
    return this.reduceEvent(value, new Bundle());
  }

  public Object reduceEvent(int value, @NonNull Bundle bundle) {

    if (value == LAUNCH_OPEN_ALARM_FRAGMENT_INTENT) {

    }
    if (value == DELETE_ALARM) {

      long id = bundle.getLong(ALARM_ID_L, -1);
      if (id == -1) {
        return null;
      }

      currentState.deleteAlarm(id);
    }
    if (value == ON_ALARM) {

      if (!bundle.containsKey("ALARM")) {
        return null;
      }

      Alarm alarm = (Alarm) bundle.get("ALARM");
      return currentState.onAlarm(alarm);

    }
    if (value == OFF_ALARM) {
      long id = bundle.getLong(ALARM_ID_L, -1);
      if (id == -1) {
        return null;
      }

      currentState.offAlarm(id);
    }

    return null;
  }

  public abstract class LaunchFragmentState {

    public void deleteAlarm(long id) {
    }

    public void loadData() {
    }

    public CreateAlarmReport onAlarm(Alarm alarm) {
      return null;
    }

    abstract public void drawState();

    public void offAlarm(long id) {
    }
  }

  public class EmptyState extends LaunchFragmentState {

    EmptyState() {
    }

    @Override
    public void loadData() {
      setCurrentState(new LoadingState());
      getCurrentState().loadData();
    }

    @Override
    public void drawState() {
      getLaunchView().drawView(false, Collections.emptyList());
    }
  }

  public class LoadingState extends LaunchFragmentState {

    LoadingState() {
      drawState();
    }

    @Override
    public void loadData() {
      asyncAlarmRepoApi.getAlarms(data -> {
        if (data == null || data.isEmpty()) {
          setCurrentState(new EmptyDataState());
        } else {
          setCurrentState(new DataState(data));
        }
      });
    }

    @Override
    public void drawState() {
      getLaunchView().drawView(true, Collections.emptyList());
    }
  }


  public class EmptyDataState extends LaunchFragmentState {

    EmptyDataState() {
      drawState();
    }

    @Override
    public void drawState() {
      getLaunchView().drawView(false, Collections.emptyList());
    }
  }

  public class DataState extends LaunchFragmentState {

    List<Alarm> alarms;

    DataState(List<Alarm> result) {
      this.alarms = result;
      drawState();
    }

    @Override
    public void deleteAlarm(long id) {
      alarmWorkApi.cancelAlarm(id);
      asyncAlarmRepoApi.deleteAlarmById(id, result -> {
        setCurrentState(new LoadingState());
        getCurrentState().loadData();
      });
    }

    @Override
    public void offAlarm(long id) {
      alarmWorkApi.cancelAlarm(id);
      asyncAlarmRepoApi.deleteAlarmById(id, result -> {
        setCurrentState(new LoadingState());
        getCurrentState().loadData();
      });
    }

    @Override
    public CreateAlarmReport onAlarm(Alarm alarm) {
      return alarmWorkApi.onAlarmFromCurrentTime(alarm);
    }

    @Override
    public void drawState() {
      getLaunchView().drawView(false, alarms);
    }
  }

  void setCurrentState(@NonNull LaunchFragmentState currentState) {
    if (currentState == null) {
      setCurrentState(new EmptyState());
      return;
    }

    this.currentState = currentState;
  }

  LaunchFragmentState getCurrentState() {
    return currentState;
  }

  LaunchView getLaunchView() {
    return launchView;
  }

  public void setLaunchView(@Nullable LaunchView launchView) {

    if (launchView == null) {
      this.launchView = new Stub();
    } else {
      this.launchView = launchView;
    }
  }
}
