package square.green.com.fragmentmanager;

import android.content.Context;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import square.green.com.fragmentmanager.dialog.RingtoneBottomSheetDialogFragment.RingtoneManagerReport;

public class AlarmRingtoneManager {

  public RingtoneManagerReport getAllSystemRingtone(Context context, String silentStringDesc) {

    final RingtoneManager ringtoneMgr = new RingtoneManager(context);
    ringtoneMgr.setType(RingtoneManager.TYPE_RINGTONE);

    Cursor alarmsCursor = ringtoneMgr.getCursor();
    final int alarmsCount = alarmsCursor.getCount();
    if (alarmsCount == 0 && !alarmsCursor.moveToFirst()) {
      alarmsCursor.close();
      return new RingtoneManagerReport(Collections.emptyList(), Collections.emptyList());
    }

    final List<CharSequence> names = new ArrayList<>(alarmsCount + 1);
    final List<String> paths = new ArrayList<>(alarmsCount + 1);

    names.add(silentStringDesc);
    paths.add("");

    while (!alarmsCursor.isAfterLast() && alarmsCursor.moveToNext()) {
      names.add(alarmsCursor.getString(1));
      paths.add(alarmsCursor.getString(2) + '/' + alarmsCursor.getLong(0));
    }
    alarmsCursor.close();

    return new RingtoneManagerReport(names, paths);
  }

  public Uri getDefaultRingtone(Context context) {
    Uri uri = RingtoneManager
        .getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE);
    return uri;
  }

}
