package square.green.com.fragmentmanager;

import static task.job.example.utils.TimeUtils.MATH_TYPE;
import static task.job.example.utils.TimeUtils.NOTIFICATION_TYPE;
import static task.job.example.utils.TimeUtils.SLIDER_TYPE;
import static task.job.example.utils.api.AlarmWorkApi.ALARM_ID_L;
import static task.job.example.utils.api.AlarmWorkApi.ALARM_TYPE_I;
import static task.job.example.utils.api.AlarmWorkApi.FROM_ALARM_B;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import java.lang.reflect.Constructor;
import java.util.List;
import square.green.com.fragmentmanager.launchFragment.LaunchFragment;

public class AppFragmentManager {

  public static final int idFragment = 6322;
  private static volatile AppFragmentManager Instance;
  private volatile boolean showingAlarm;

  public static AppFragmentManager getInstance() {
    AppFragmentManager localInstance = Instance;
    if (localInstance == null) {
      synchronized (AppFragmentManager.class) {
        localInstance = Instance;
        if (localInstance == null) {
          Instance = localInstance = new AppFragmentManager();
        }
      }
    }
    return localInstance;
  }

  // New sector

  public void loadFragment(FragmentManager fragmentManager, @NonNull String className,
      int containerId, @NonNull Bundle arguments) {

    try {
      Fragment fragment = fragmentManager.findFragmentByTag(className);
      if (fragment == null) {
        Constructor constructor = Class.forName(className).getConstructor();
        fragment = (Fragment) constructor.newInstance();
        fragment.setArguments(arguments);
      }

      FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
      fragmentTransaction.replace(containerId, fragment, fragment.getClass().getSimpleName());
      fragmentTransaction.commit();

    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }

  public void showAlarmTaskFragment(FragmentManager fragmentManager, int type, int idContainer,
      Bundle arguments) {

    if (isShowingAlarm()) {
      return;
    }

    setShowingAlarm(true);

    if (type == SLIDER_TYPE || type == NOTIFICATION_TYPE) {
      loadFragment(fragmentManager, SliderFragment.class.getName(), idContainer, arguments);
    }

    if (type == MATH_TYPE) {
      loadFragment(fragmentManager, MathFragment.class.getName(), idContainer, arguments);
    }
  }

  // Old sector

  public void closeAlarm(FragmentActivity activity) {
    setShowingAlarm(false);
    popBack(activity);
  }

  private void popBack(FragmentActivity activity) {
    if (activity.getSupportFragmentManager().getBackStackEntryCount() == 0) {
      activity.finish();
      return;
    }

    FragmentManager fragmentManager = activity.getSupportFragmentManager();

    if (fragmentManager.getBackStackEntryCount() == 0) {
      if (isShowingAlarm()) {
        activity.moveTaskToBack(true);
      }
    }

    activity.getSupportFragmentManager().popBackStack();
  }

  public void onCreate(FragmentManager fragmentManager, Intent intent) {

    List<Fragment> fragments = fragmentManager.getFragments();

    if (fragments.isEmpty()) {

      boolean isFromAlarm = intent.getBooleanExtra(FROM_ALARM_B, false);
      if (isFromAlarm) {
        //long idAlarm = intent.getLongExtra(ALARM_ID_L, -1);
        int type = intent.getIntExtra(ALARM_TYPE_I, -1);

        showAlarmTaskFragment(fragmentManager, type, idFragment, intent.getExtras());
        return;
      }

      loadFragment(fragmentManager, LaunchFragment.class.getName(), idFragment, null);
    } else {

      for (Fragment fragment : fragments) {
        loadFragment(fragmentManager, fragment.getClass().getName(), fragment.getId(),
            fragment.getArguments());
      }
    }
  }


  public void onNewIntent(FragmentManager fragmentManager, Intent intent) {

    long idAlarm = intent.getLongExtra(ALARM_ID_L, 0);
    int type = intent.getIntExtra(ALARM_TYPE_I, 0);

    showAlarmTaskFragment(fragmentManager, type, idFragment, intent.getExtras());
  }

  private boolean isShowingAlarm() {
    return showingAlarm;
  }

  private void setShowingAlarm(boolean showingAlarm) {
    this.showingAlarm = showingAlarm;
  }

}
