package square.green.com.fragmentmanager;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Random;


public class MathFragment extends AlarmTaskFragment {

    InputMethodManager imm;

    private String alarmEquation;
    private String alarmAnswer;
    private TextView equation;
    private EditText answer;
    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (alarmAnswer == null)
                return;

            if (s.toString().equals(alarmAnswer))
                switchOffAlarm();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    public static String getMathEquation() {

        String equation[] = {
                "(122+18):70=2",
                "(64:8+20):7=4",
                "20·(26+14):100=8",
                "1·(30+2)–4·4=16",
                "5·4+12=32",
                "(400–300)–36=64"
        };

        Random random = new Random();
        return equation[random.nextInt(equation.length)];
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.math_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        equation = view.findViewById(R.id.equation_math_fragment);
        answer = view.findViewById(R.id.answer_math_fragment);

        equation.setText(alarmEquation);



    }

    @Override
    public void onStart() {
        super.onStart();

        answer.addTextChangedListener(textWatcher);

        setAlarmEquation(getMathEquation());
        answer.setEnabled(true);
        answer.requestFocus();
        equation.setText(alarmEquation);

        if(getActivity() == null)
            return;
        imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        if(imm != null)
            imm.showSoftInput(answer, InputMethodManager.SHOW_IMPLICIT);




    }

    @Override
    public void onStop() {
        super.onStop();
        answer.removeTextChangedListener(textWatcher);

        answer.clearFocus();
        if(imm != null && imm.isActive())
            imm.hideSoftInputFromWindow(answer.getWindowToken(), 0);


    }

    private void setAlarmEquation(String alarmEquation) {

        int point = alarmEquation.lastIndexOf('=') + 1;

        this.alarmEquation = alarmEquation.substring(0, point);
      this.alarmAnswer = alarmEquation.substring(point);
    }
}
