package square.green.com.fragmentmanager.launchFragment;

import square.green.com.di.ApplicationComponent;
import task.job.example.utils.api.AlarmWorkLiveApi;

public class LaunchComponentImpl {


  private static volatile LaunchComponentImpl Instance;
  private ApplicationComponent applicationComponent;

  public static LaunchComponentImpl getInstance() {
    LaunchComponentImpl localInstance = Instance;
    if (localInstance == null) {
      synchronized (LaunchComponentImpl.class) {
        localInstance = Instance;
        if (localInstance == null) {
          Instance = localInstance = new LaunchComponentImpl();
        }
      }
    }
    return localInstance;
  }

  public AlarmWorkLiveApi provideAlarmWorkLiveApi() {
    return null;

  }
}
