package task.job.example.utils.response;


import androidx.work.Data;
import java.util.UUID;

public class CreateAlarmReport {

  private Data data;
  UUID id;
  boolean result;
  long time;

  public CreateAlarmReport(UUID id, boolean result, long time, Data myData) {
    this.id = id;
    this.result = result;
    this.time = time;
    this.data = myData;
  }

  public UUID getId() {
    return id;
  }

  public boolean isResult() {
    return result;
  }

  public long getTime() {
    return time;
  }

  public Data getData() {
    return data;
  }

  @Override
  public String toString() {
    return "CreateAlarmReport{" +
        "data=" + data +
        ", id=" + id +
        ", result=" + result +
        ", time=" + time +
        '}';
  }
}
