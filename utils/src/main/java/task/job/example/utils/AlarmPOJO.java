package task.job.example.utils;

public class AlarmPOJO extends AlarmForList {

  String ringtone;
  boolean isVibrate;
  String answer;


  public AlarmPOJO(AlarmForList alarmForList, String ringtone, boolean vibrate) {
    super(alarmForList);
    this.ringtone = ringtone;
    this.isVibrate = vibrate;
  }

  public boolean isVibrate() {
    return isVibrate;
  }

  public String getRingtone() {
    return ringtone;
  }

  public void setRingtone(String ringtone) {
    this.ringtone = ringtone;
  }

  public void setVibrate(boolean vibrate) {
    isVibrate = vibrate;
  }

  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  public void setId(long id) {
    super.setId(id);
  }

}
