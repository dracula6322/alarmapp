package task.job.example.utils.api;

import android.content.Context;
import task.job.example.utils.model.Alarm;
import task.job.example.utils.response.CreateAlarmReport;

public interface AlarmWorkApi {

  String ALARM_DAYS_I = "ALARM_DAYS_I";
  String FROM_ALARM_B = "FROM_ALARM_B";
  String TIME_TRIGGER_ALARM_L = "TIME_TRIGGER_ALARM_L";
  String ALARM_TYPE_I = "ALARM_TYPE_I";
  String ALARM_ID_L = "ALARM_ID_L";

  CreateAlarmReport onAlarmFromCurrentTime(Alarm alarm);

  void cancelAlarm(long id);

  CreateAlarmReport setAlarm(int type, int currentDays, int currentHour,
      int currentMinute, int alarmDays, int alarmHour, int alarmMinute, long idAlarm,
      long launchTime);

  void stopService(Context context);
}
