package task.job.example.utils.api;

import android.content.Context;

public interface ApplicationApi {
    Context getContext();
}
