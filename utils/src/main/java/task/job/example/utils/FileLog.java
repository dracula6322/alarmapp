package task.job.example.utils;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import square.green.com.utils.BuildConfig;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class FileLog {

  private OutputStreamWriter streamWriter = null;
  private BackgroundThread logQueue = null;

  private static volatile FileLog Instance = null;

  public static FileLog getInstance() {
    FileLog localInstance = Instance;
    if (localInstance == null) {
      synchronized (FileLog.class) {
        localInstance = Instance;
        if (localInstance == null) {
          Instance = localInstance = new FileLog();
        }
      }
    }
    return localInstance;
  }

  public void init(Context context) {

    if (!BuildConfig.DEBUG) {
      return;
    }
    File currentFile = null;
    try {
      File sdCard = context.getExternalFilesDir(null);
      if (sdCard == null) {
        return;
      }
      File dir = new File(sdCard.getAbsolutePath() + "/logs");
      dir.mkdirs();
      currentFile = new File(dir, getDate(System.currentTimeMillis()) + ".txt");
    } catch (Exception e) {
      e.printStackTrace();
    }
    try {
      logQueue = new BackgroundThread("logQueue");
      if (currentFile == null) {
        return;
      }
      currentFile.createNewFile();
      FileOutputStream stream = new FileOutputStream(currentFile);
      streamWriter = new OutputStreamWriter(stream);
      streamWriter.write("-----start log " + getDate(System.currentTimeMillis()) + "-----\n");
      streamWriter.flush();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void v(final String message) {
    this.v("TAG", message);
  }

  public void v(final String tag, final String message) {
    if (!BuildConfig.DEBUG) {
      return;
    }

    Log.v(tag, message);

    if (getInstance().streamWriter != null) {
      getInstance().logQueue.postRunnable(() -> {
        try {
          getInstance().streamWriter
              .write(getDate(System.currentTimeMillis()) + " D: " + message + "\n");
          getInstance().streamWriter.flush();
        } catch (Exception e) {
          e.printStackTrace();
        }
      });
    }
  }

  private String getDate(long time) {
    return android.text.format.DateFormat.format("dd_MM_yyyy_HH_mm_ss", time).toString();
  }

}