package task.job.example.utils;

import android.content.res.Resources;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import square.green.com.utils.R;

public class TimeUtils {

  final static public CharSequence[] works = {"Слайдер",
      "Уведомление",
      "Мат. задача"};

  private static int exerciseCount = 0;
  public static final int SLIDER_TYPE = exerciseCount++;
  public static final int NOTIFICATION_TYPE = exerciseCount++;
  public static final int MATH_TYPE = exerciseCount++;

  static public String getWeekString(Resources resources, int week) {

    if (resources == null) {
      return "";
    }

    CharSequence[] shortWeek = resources.getStringArray(R.array.week_abbreviation);

    if (week == 127) {
      return resources.getString(R.string.every_day);
    }

    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < shortWeek.length; i++) {

      if ((week & (int) Math.pow(2, i)) > 0) {
        stringBuilder.append(shortWeek[i]).append(' ');
      }
    }

    String result = stringBuilder.toString();
    if (result.isEmpty()) {
      return resources.getString(R.string.never_repeat);
    }

    return result;
  }

  static public String stringTimeToAlarm(long time, boolean isShowSecond) {

    long second = (time - System.currentTimeMillis()) / 1000;

    if (second <= 0) {
      return "< 1 мин.";
    }

    long minute = second / 60;
    second = second % 60;
    long hour = minute / 60;
    minute -= hour * 60;
    long day = hour / 24;
    hour -= day * 24;

    StringBuilder stringBuffer = new StringBuilder();
    if (day != 0) {
      stringBuffer.append(day).append(" д. ");
    }
    if (hour != 0) {
      stringBuffer.append(hour).append(" ч. ");
    }
    if (minute != 0) {
      stringBuffer.append(minute).append(" мин. ");
    }

    if (stringBuffer.length() == 0) {
      if (second > 8) {
        stringBuffer.append("1 мин. ");
      } else {
        stringBuffer.append("< 1 мин. ");
      }
    }

    if (isShowSecond && second != 0) {
      stringBuffer.append(second).append(" сек.");
    }

    return stringBuffer.toString();
  }

  static public String stringTimeToAlarmDebug(long time, boolean isShowSecond) {

    long second = (time) / 1000;

    long minute = second / 60;
    second = second % 60;
    long hour = minute / 60;
    minute -= hour * 60;
    long day = hour / 24;
    hour -= day * 24;

    StringBuilder stringBuffer = new StringBuilder();
    if (day != 0) {
      stringBuffer.append(day).append(" д. ");
    }
    if (hour != 0) {
      stringBuffer.append(hour).append(" ч. ");
    }
    if (minute != 0) {
      stringBuffer.append(minute).append(" мин. ");
    }

    stringBuffer.append(second).append(" сек.");

    return stringBuffer.toString();
  }

  static public long getNearestDay(int currentDay, int currentHour, int currentMinute,
      int alarmDays, int alarmHour, int alarmMinute) {

    Calendar currentCalendar = new GregorianCalendar();
    currentCalendar.setLenient(false);
    currentCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
    currentCalendar.setTimeInMillis(0);
    currentCalendar.set(Calendar.HOUR_OF_DAY, currentHour);
    currentCalendar.set(Calendar.MINUTE, currentMinute);
    long calendarTimeInMillis = currentCalendar.getTimeInMillis();

    GregorianCalendar alarmCalendar = new GregorianCalendar();
    currentCalendar.setLenient(false);
    alarmCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
    alarmCalendar.setTimeInMillis(0);
    alarmCalendar.set(Calendar.HOUR_OF_DAY, alarmHour);
    alarmCalendar.set(Calendar.MINUTE, alarmMinute);

    ArrayList<Long> times = new ArrayList<>();
    for (int i = 0; i < 8; i++) {
      times.add(alarmCalendar.getTimeInMillis());
      alarmCalendar.roll(Calendar.DAY_OF_YEAR, true);
    }

    ArrayList<Boolean> results = new ArrayList<>();
    for (int i = 0; i < times.size(); i++) {
      results.add(!compareTo(calendarTimeInMillis, times.get(i)));
    }

    int bitwiseCount = TimeUtils.bitwiseFromCalendarDay(currentDay);


    BitSet bitSet = new BitSet(7);
    for (int i = 0; i < 7; i++) {
      bitSet.set(i, (alarmDays & ((int) Math.pow(2, i))) != 0);
    }

    if (bitwiseCount != 0) {
      boolean tmp = bitSet.get(bitwiseCount);
      int previous = bitwiseCount;
      for (int i = 0; i < 6; i++) {
        int next = (previous + bitwiseCount) < 7 ? (previous + bitwiseCount)
            : (previous + bitwiseCount - 7);
        bitSet.set(previous, bitSet.get(next));
        previous = next;
      }
      bitSet.set(0, tmp);
    }

    if (bitSet.isEmpty()) {
      bitSet.set(0, true);
      bitSet.set(1, true);
    }

    long result = -1;
    for (int i = 0; i < 7; i++) {

      if (bitSet.get(i) && results.get(i)) {
        result = times.get(i);
        break;
      }
    }
    if (result == -1) {
      result = times.get(results.size() - 1);
    }

    return result - currentCalendar.getTimeInMillis();// + 8 * 1000;
  }

  public static int bitwiseFromCalendarDay(int calendarDay) {

    switch (calendarDay) {
      case Calendar.MONDAY:
        return 0;
      case Calendar.TUESDAY:
        return 1;
      case Calendar.WEDNESDAY:
        return 2;
      case Calendar.THURSDAY:
        return 3;
      case Calendar.FRIDAY:
        return 4;
      case Calendar.SATURDAY:
        return 5;
      default:
        return 6;
    }
  }

  public static int dayFromCalendar(int calendarDay) {

    switch (calendarDay) {
      case Calendar.MONDAY:
        return (int) Math.pow(2, 0);
      case Calendar.TUESDAY:
        return (int) Math.pow(2, 1);
      case Calendar.WEDNESDAY:
        return (int) Math.pow(2, 2);
      case Calendar.THURSDAY:
        return (int) Math.pow(2, 3);
      case Calendar.FRIDAY:
        return (int) Math.pow(2, 4);
      case Calendar.SATURDAY:
        return (int) Math.pow(2, 5);
      default:
        return (int) Math.pow(2, 6);
    }
  }
  public static int dayFromIndexToCalendar(int indexDay) {

    switch (indexDay) {
      case 0:
        return Calendar.MONDAY;
      case 1:
        return Calendar.TUESDAY;
      case 2:
        return Calendar.WEDNESDAY;
      case 3:
        return Calendar.THURSDAY;
      case 4:
        return Calendar.FRIDAY;
      case 5:
        return Calendar.SATURDAY;
      default:
        return Calendar.SUNDAY;
    }
  }

  private static boolean compareTo(long currentTime, long t) {
    int result = Long.compare(currentTime, t);
    return result > 0;
  }

}
