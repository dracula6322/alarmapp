package task.job.example.utils.api;

import android.content.Context;
import androidx.lifecycle.MutableLiveData;
import task.job.example.utils.model.Alarm;

public interface AlarmWorkLiveApi {

  boolean startAlarmService(Context context, Alarm alarmForService, int type);

  MutableLiveData<Long> getLiveDataWorking();

  boolean stopAlarmService(Context context);
}
