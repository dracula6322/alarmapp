package task.job.example.utils.model;

import static task.job.example.utils.model.Alarm.ALARM_OFF;

final public class AlarmBuilder {

  private int answerType;
  private String answer;
  private long id = 0;
  private int hour = 0;
  private int minute = 0;
  private int days = 1 + 2 + 4 + 8 + 16;
  private int state = ALARM_OFF;
  private boolean isVibrate;
  private String ringtone;
  private long time;

  private AlarmBuilder() {
  }

  public static AlarmBuilder anAlarm(Alarm alarm) {
    return new AlarmBuilder()
        .withAnswerType(alarm.getAnswerType())
        .withAnswer(alarm.getAnswer())
        .withId(alarm.getId())
        .withHour(alarm.getHour())
        .withMinute(alarm.getMinute())
        .withDays(alarm.getDays())
        .withState(alarm.getState())
        .withIsVibrate(alarm.isVibrate())
        .withRingtone(alarm.getRingtone())
        .withTime(alarm.getTime());
  }

  public static AlarmBuilder anAlarm() {
    return new AlarmBuilder();
  }

  public AlarmBuilder withAnswerType(int answerType) {
    this.answerType = answerType;
    return this;
  }

  public AlarmBuilder withAnswer(String answer) {
    this.answer = answer;
    return this;
  }

  public AlarmBuilder withId(long id) {
    this.id = id;
    return this;
  }

  public AlarmBuilder withHour(int hour) {
    this.hour = hour;
    return this;
  }

  public AlarmBuilder withMinute(int minute) {
    this.minute = minute;
    return this;
  }

  public AlarmBuilder withDays(int days) {
    this.days = days;
    return this;
  }

  public AlarmBuilder withState(int state) {
    this.state = state;
    return this;
  }

  public AlarmBuilder withIsVibrate(boolean isVibrate) {
    this.isVibrate = isVibrate;
    return this;
  }

  public AlarmBuilder withRingtone(String ringtone) {
    this.ringtone = ringtone;
    return this;
  }

  public AlarmBuilder withTime(long time) {
    this.time = time;
    return this;
  }

  public Alarm build() {
    Alarm alarm = new Alarm(id, hour, minute, days, state, isVibrate, ringtone, time);
    alarm.setAnswerType(answerType);
    alarm.setAnswer(answer);
    return alarm;
  }

  public static AlarmBuilder generateDefaultAlarmBuilder(long id) {

    return AlarmBuilder.anAlarm()
        .withId(id)
        .withRingtone("ololo")
        .withDays(22)
        .withHour(21)
        .withMinute(47)
        .withAnswer("qwe")
        .withAnswerType(2)
        .withIsVibrate(true);
  }

  public static void compareWithDefaultAlarm(Alarm alarm) {

  }

  public int getMinute() {
    return minute;
  }

  public int getHour() {
    return hour;
  }

  public int getDays() {
    return days;
  }

  public String getRingtone() {
    return ringtone;
  }


  public boolean isVibrate() {
    return isVibrate;
  }

  public int getType() {
    return answerType;
  }
}
