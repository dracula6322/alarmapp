package task.job.example.utils

import android.os.Handler
import android.os.Looper
import java.util.concurrent.CountDownLatch

class BackgroundThread(name : String) : Thread(name) {

    private val countDownLatch = CountDownLatch(1)

    @Volatile
    private var handler: Handler? = null

    init {
        start()
    }

    override fun run() {
        super.run()
        Looper.prepare()
        handler = Handler()
        countDownLatch.countDown()
        Looper.loop()
    }

    fun postRunnable(runnable: Runnable): Boolean {
        try {
            countDownLatch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
            return false
        }

        return handler?.post(runnable) ?: false
    }

    fun postRunnable(runnable: Runnable, time: Long) {
        try {
            countDownLatch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
            return
        }

        handler?.postDelayed(runnable, time)
    }

    fun stopThread() {
        try {
            countDownLatch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
            return
        }

        handler?.removeCallbacksAndMessages(null)
    }

}