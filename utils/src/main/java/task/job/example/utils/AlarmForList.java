package task.job.example.utils;

import android.os.Parcel;
import android.os.Parcelable;

public class AlarmForList implements Parcelable {

  private int state;
  private long id;
  private long time;
  private int hour;
  private int minute;
  private int days;
  private int type;

  public AlarmForList() {
  }

  public AlarmForList(AlarmForList alarmForList) {
    this.state = alarmForList.state;
    this.id = alarmForList.id;
    this.time = alarmForList.time;
    this.hour = alarmForList.getHour();
    this.minute = alarmForList.getMinute();
    this.days = alarmForList.getDays();
    this.type = alarmForList.getType();
  }

  public AlarmForList(int state, long id, long time, int hour, int minute, int days, int type) {
    this.state = state;
    this.id = id;
    this.time = time;
    this.hour = hour;
    this.minute = minute;
    this.days = days;
    this.type = type;
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(state);
    dest.writeLong(id);
    dest.writeLong(time);
    dest.writeInt(hour);
    dest.writeInt(minute);
    dest.writeInt(days);
    dest.writeInt(type);
  }

  protected AlarmForList(Parcel in) {
    state = in.readInt();
    id = in.readLong();
    time = in.readLong();
    hour = in.readInt();
    minute = in.readInt();
    days = in.readInt();
    type = in.readInt();
  }

  public static final Creator<AlarmForList> CREATOR = new Creator<AlarmForList>() {
    @Override
    public AlarmForList createFromParcel(Parcel in) {
      return new AlarmForList(in);
    }

    @Override
    public AlarmForList[] newArray(int size) {
      return new AlarmForList[size];
    }
  };

  public int getState() {
    return state;
  }

  public long getId() {
    return id;
  }

  public long getTime() {
    return time;
  }

  public int getHour() {
    return hour;
  }

  public int getMinute() {
    return minute;
  }

  public int getDays() {
    return days;
  }

  public int getType() {
    return type;
  }

  public void setState(int state) {
    this.state = state;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setTime(long time) {
    this.time = time;
  }

  public void setHour(int hour) {
    this.hour = hour;
  }

  public void setMinute(int minute) {
    this.minute = minute;
  }

  public void setDays(int days) {
    this.days = days;
  }

  public void setType(int type) {
    this.type = type;
  }
}
