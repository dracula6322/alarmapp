package task.job.example.utils

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.content.res.TypedArray
import android.graphics.*
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.ViewOutlineProvider
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import square.green.com.utils.R

class SliderView(context: Context,
                     attrs: AttributeSet?,
                     defStyleAttr: Int) : View(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null, R.styleable.SlideToActViewTheme_slideToActViewStyle)
    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, R.styleable.SlideToActViewTheme_slideToActViewStyle)

    /* -------------------- LAYOUT BOUNDS -------------------- */

    private var mDesiredSliderHeightDp: Float = 72F
    private var mDesiredSliderWidthDp: Float = 280F
    private var mDesiredSliderHeight: Int = 0
    private var mDesiredSliderWidth: Int = 0

    /* -------------------- MEMBERS -------------------- */

    /** Height of the drawing area */
    private var mAreaHeight: Int = 0
    /** Width of the drawing area */
    private var mAreaWidth: Int = 0
    /** Actual Width of the drawing area, used for animations */
    private var mActualAreaWidth: Int = 0
    /** Border Radius, default to mAreaHeight/2, -1 when not initialized */
    private var mBorderRadius: Int = -1
    /** Margin of the cursor from the outer area */
    private var mActualAreaMargin: Int
    private val mOriginAreaMargin: Int

    /** Text message */
    var text: CharSequence? = ""
        set(value) {
            field = value
            invalidate()
        }

    /** Typeface for the text field */
    private var typeFace = Typeface.NORMAL
        set(value) {
            field = value
            mTextPaint.typeface = Typeface.create("sans-serif-light" , value)
            invalidate()
        }

    /** Size for the text message */
    private val mTextSize: Int

    private var mTextYPosition = -1f
    private var mTextXPosition = -1f

    /** Outer color used by the slider (primary) */
    private var outerColor: Int = 0
        set(value) {
            field = value
            mOuterPaint.color = value
            //mDrawableArrow.setTint(value)
            invalidate()
        }

    /** Inner color used by the slider (secondary, icon and border) */
    private var innerColor: Int = 0
        set(value) {
            field = value
            mInnerPaint.color = value
            mTextPaint.color = value
            invalidate()
        }

    /** Slider cursor position (between 0 and (`reaWidth - mAreaHeight)) */
    private var mPosition: Int = 0
        set(value) {
            field = value
            if (mAreaWidth - mAreaHeight == 0) {
                // Avoid 0 division
                mPositionPerc = 0f
                mPositionPercInv = 1f
                return
            }
            mPositionPerc = value.toFloat() / (mAreaWidth - mAreaHeight).toFloat()
            mPositionPercInv = 1 - value.toFloat() / (mAreaWidth - mAreaHeight).toFloat()
        }
    /** Slider cursor position in percentage (between 0f and 1f) */
    private var mPositionPerc: Float = 0f
    /** 1/mPositionPerc */
    private var mPositionPercInv: Float = 1f

    /* -------------------- ICONS -------------------- */

    private val mIconMargin: Int
    /** Margin for Arrow Icon */
    private var mArrowMargin: Int
    /** Current angle for Arrow Icon */
    private var mArrowAngle: Float = 0f
    /** Margin for Tick Icon */
    private var mTickMargin: Int

    /** Arrow drawable */
    private val mDrawableArrow: Drawable

    /** The icon for the drawable */
    private var mIcon: Int = R.drawable.ic_arrow

    /* -------------------- PAINT & DRAW -------------------- */
    /** Paint used for outer elements */
    private val mOuterPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    /** Paint used for inner elements */
    private val mInnerPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    /** Paint used for text elements */
    private val mTextPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    /** Inner rectangle (used for arrow rotation) */
    private var mInnerRect: RectF
    /** Outer rectangle (used for area drawing) */
    private var mOuterRect: RectF
    /** Grace value, when mPositionPerc > mGraceValue slider will perform the 'complete' operations */
    private val mGraceValue: Float = 0.8F

    /** Last X coordinate for the touch event */
    private var mLastX: Float = 0F
    /** Flag to understand if user is moving the slider cursor */
    private var mFlagMoving: Boolean = false

    /** Public flag to lock the slider */
    private var isLocked = false

    var onSlideCompleteListener: OnSlideCompleteListener? = null

    init {
        val actualOuterColor : Int
        val actualInnerColor : Int

        val layoutAttrs: TypedArray = context.theme.obtainStyledAttributes(attrs,
                R.styleable.SlideToActView, defStyleAttr, R.style.SlideToActView)
        try {
            mDesiredSliderHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, mDesiredSliderHeightDp, resources.displayMetrics).toInt()
            mDesiredSliderWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, mDesiredSliderWidthDp, resources.displayMetrics).toInt()
            mDesiredSliderHeight = layoutAttrs.getDimensionPixelSize(R.styleable.SlideToActView_slider_height, mDesiredSliderHeight)

            mBorderRadius = layoutAttrs.getDimensionPixelSize(R.styleable.SlideToActView_border_radius, -1)

            val defaultOuter = ContextCompat.getColor(this.context, R.color.defaultAccent)
            val defaultInner = ContextCompat.getColor(this.context, R.color.white)
            actualOuterColor = layoutAttrs.getColor(R.styleable.SlideToActView_outer_color, defaultOuter)
            actualInnerColor = layoutAttrs.getColor(R.styleable.SlideToActView_inner_color, defaultInner)
            text = layoutAttrs.getString(R.styleable.SlideToActView_text)
            typeFace = layoutAttrs.getInt(R.styleable.SlideToActView_text_style, 0)

            isLocked = layoutAttrs.getBoolean(R.styleable.SlideToActView_slider_locked, false)

            mTextSize = layoutAttrs.getDimensionPixelSize(R.styleable.SlideToActView_text_size, resources.getDimensionPixelSize(R.dimen.default_text_size))
            mOriginAreaMargin = layoutAttrs.getDimensionPixelSize(R.styleable.SlideToActView_area_margin, resources.getDimensionPixelSize(R.dimen.default_area_margin))
            mActualAreaMargin = mOriginAreaMargin

            mIcon = layoutAttrs.getResourceId(R.styleable.SlideToActView_slider_icon, R.drawable.ic_arrow)
        } finally {
            layoutAttrs.recycle()
        }

        mInnerRect = RectF((mActualAreaMargin + mPosition).toFloat(), mActualAreaMargin.toFloat(),
                (mAreaHeight + mPosition).toFloat() - mActualAreaMargin.toFloat(),
                mAreaHeight.toFloat() - mActualAreaMargin.toFloat())

        mOuterRect = RectF(mActualAreaWidth.toFloat(), 0f, mAreaWidth.toFloat() - mActualAreaWidth.toFloat(), mAreaHeight.toFloat())

        mDrawableArrow = parseVectorDrawableCompat(context.resources, mIcon, context.theme)

        mTextPaint.textAlign = Paint.Align.CENTER
        mTextPaint.textSize = mTextSize.toFloat()

        outerColor = actualOuterColor
        innerColor = actualInnerColor

        mIconMargin = context.resources.getDimensionPixelSize(R.dimen.default_icon_margin)
        mArrowMargin = mIconMargin
        mTickMargin = mIconMargin

        // This outline provider force removal of shadow
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            outlineProvider = SlideToActOutlineProvider()
        }
    }

    private fun parseVectorDrawableCompat(res: Resources, resId: Int, theme: Resources.Theme): Drawable {
        return ResourcesCompat.getDrawable(res, resId, theme)!!
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val width: Int

        width = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> Math.min(mDesiredSliderWidth, widthSize)
            MeasureSpec.UNSPECIFIED -> mDesiredSliderWidth
            else -> mDesiredSliderWidth
        }
        setMeasuredDimension(width, mDesiredSliderHeight)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        mAreaWidth = w
        mAreaHeight = h
        if (mBorderRadius == -1) // Round if not set up
            mBorderRadius = h / 2

        // Text horizontal/vertical positioning (both centered)
        mTextXPosition = mAreaWidth.toFloat() / 2
        mTextYPosition = (mAreaHeight.toFloat() / 2) - (mTextPaint.descent() + mTextPaint.ascent()) / 2
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (canvas == null) return

        // Outer area
        mOuterRect.set(mActualAreaWidth.toFloat(), 0f, mAreaWidth.toFloat() - mActualAreaWidth.toFloat(), mAreaHeight.toFloat())
        canvas.drawRoundRect(mOuterRect, mBorderRadius.toFloat(), mBorderRadius.toFloat(), mOuterPaint)

        // Inner Cursor
        // ratio is used to compute the proper border radius for the inner rect (see #8).
        val ratio = (mAreaHeight - 2 * mActualAreaMargin).toFloat() / mAreaHeight.toFloat()
        mInnerRect.set((mActualAreaMargin + mPosition).toFloat(),
                mActualAreaMargin.toFloat(),
                (mAreaHeight + mPosition).toFloat() - mActualAreaMargin.toFloat(),
                mAreaHeight.toFloat() - mActualAreaMargin.toFloat())
        canvas.drawRoundRect(mInnerRect, mBorderRadius.toFloat() * ratio, mBorderRadius.toFloat() * ratio, mInnerPaint)

        // Text alpha
        mTextPaint.alpha = (255 * mPositionPercInv).toInt()

        // Vertical + Horizontal centering
        canvas.drawText(text.toString(), mTextXPosition, mTextYPosition, mTextPaint)

        // Arrow angle
        mArrowAngle = -180 * mPositionPerc
        canvas.rotate(mArrowAngle, mInnerRect.centerX(), mInnerRect.centerY())
        mDrawableArrow.setBounds(mInnerRect.left.toInt() + mArrowMargin,
                mInnerRect.top.toInt() + mArrowMargin,
                mInnerRect.right.toInt() - mArrowMargin,
                mInnerRect.bottom.toInt() - mArrowMargin)
        if (mDrawableArrow.bounds.left <= mDrawableArrow.bounds.right &&
                mDrawableArrow.bounds.top <= mDrawableArrow.bounds.bottom) {
            mDrawableArrow.draw(canvas)
        }
        canvas.rotate(-1 * mArrowAngle, mInnerRect.centerX(), mInnerRect.centerY())
    }

    fun touchDownEvent(xPosition: Float, yPosition: Float) {

        if (checkInsideButton(xPosition, yPosition)) {
            mFlagMoving = true
            mLastX = xPosition
            parent.requestDisallowInterceptTouchEvent(true)
        }
    }

    fun touchUpEvent() {

        mFlagMoving = false
        parent.requestDisallowInterceptTouchEvent(false)
        if ((mPosition > 0 && isLocked) || (mPosition > 0 && mPositionPerc < mGraceValue)) {
            // Check for grace value

            val positionAnimator = ValueAnimator.ofInt(mPosition, 0)
            positionAnimator.duration = 300
            positionAnimator.addUpdateListener {
                mPosition = it.animatedValue as Int
                invalidateArea()
            }
            positionAnimator.start()
        } else if (mPosition > 0 && mPositionPerc >= mGraceValue) {
            isEnabled = false // Fully disable touch events
            onSlideCompleteListener?.onSlideComplete()
        }
    }

    fun touchMoveEvent(xPosition: Float) {

        if (mFlagMoving) {
            val diffX = xPosition - mLastX
            mLastX = xPosition
            increasePosition(diffX.toInt())
            invalidateArea()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null && isEnabled) {
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    touchDownEvent(event.x, event.y)
                }
                MotionEvent.ACTION_UP -> {
                    touchUpEvent()
                }
                MotionEvent.ACTION_MOVE -> {
                    touchMoveEvent(event.x)
                }
            }
            return true
        }
        return super.onTouchEvent(event)
    }

    private fun invalidateArea() {
        invalidate()
        //invalidate(mOuterRect.left.toInt(), mOuterRect.top.toInt(), mOuterRect.right.toInt(), mOuterRect.bottom.toInt())
    }

    /**
     * Private method to check if user has touched the slider cursor
     * @param x The x coordinate of the touch event
     * @param y The y coordinate of the touch event
     * @return A boolean that informs if user has pressed or not
     */
    private fun checkInsideButton(x: Float, y: Float): Boolean {
        return (0 < y && y < mAreaHeight && mPosition < x && x < (mAreaHeight + mPosition))
    }

    /**
     * Private method for increasing/decreasing the position
     * Ensure that position never exits from its range [0, (mAreaWidth - mAreaHeight)]
     *
     * @param inc Increment to be performed (negative if it's a decrement)
     */
    private fun increasePosition(inc: Int) {
        mPosition += inc
        if (mPosition < 0)
            mPosition = 0
        if (mPosition > (mAreaWidth - mAreaHeight))
            mPosition = mAreaWidth - mAreaHeight
    }

    /**
     * Event handler for the slide complete event.
     * Use this handler to react to slide event
     */
    interface OnSlideCompleteListener {
        fun onSlideComplete()
    }
    /**
     * Outline provider for the SlideToActView.
     * This outline will suppress the shadow (till the moment when Android will support
     * updatable Outlines).
     */
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private inner class SlideToActOutlineProvider : ViewOutlineProvider() {

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun getOutline(view: View?, outline: Outline?) {
            if (view == null || outline == null)
                return

            outline.setRoundRect(mActualAreaWidth, 0, mAreaWidth - mActualAreaWidth, mAreaHeight,mBorderRadius.toFloat())
        }
    }
}



