package task.job.example.utils.model;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;

@Entity
public final class Alarm implements Parcelable {

  @PrimaryKey
  private long id = 0;
  private int hour = 0;
  private int minute = 0;
  private int days = 1 + 2 + 4 + 8 + 16;
  private int state = ALARM_OFF;
  private boolean isVibrate = true;

  @NotNull
  private String ringtone;
  private long time;
  public static final int ALARM_OFF = 1;
  public static final int ALARM_ON = 2;


  private int answerType;
  private String answer;

  public Alarm() {
  }

  public Alarm(long id, int hour, int minute, int days, int state, boolean isVibrate,
      @NotNull String ringtone, long time) {
    this.id = id;
    this.hour = hour;
    this.minute = minute;
    this.days = days;
    this.state = state;
    this.isVibrate = isVibrate;
    this.ringtone = ringtone;
    this.time = time;
  }

  public Alarm(@NotNull Alarm alarm) {
    this(alarm.id, alarm.hour, alarm.minute, alarm.days, alarm.state, alarm.isVibrate,
        alarm.ringtone, alarm.time);
  }


  protected Alarm(Parcel in) {
    id = in.readLong();
    hour = in.readInt();
    minute = in.readInt();
    days = in.readInt();
    state = in.readInt();
    isVibrate = in.readByte() != 0;
    ringtone = in.readString();
    time = in.readLong();
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public int getHour() {
    return hour;
  }

  public void setHour(int hour) {
    this.hour = hour;
  }

  public int getMinute() {
    return minute;
  }

  public void setMinute(int minute) {
    this.minute = minute;
  }

  public int getDays() {
    return days;
  }

  public void setDays(int days) {
    this.days = days;
  }

  public int getState() {
    return state;
  }

  public void setState(int state) {
    this.state = state;
  }

  public boolean isVibrate() {
    return isVibrate;
  }

  public void setVibrate(boolean vibrate) {
    isVibrate = vibrate;
  }

  @NotNull
  public String getRingtone() {
    return ringtone;
  }

  public void setRingtone(@NotNull String ringtone) {
    this.ringtone = ringtone;
  }

  public long getTime() {
    return time;
  }

  public void setTime(long time) {
    this.time = time;
  }

  public int getAnswerType() {
    return answerType;
  }

  public void setAnswerType(int answerType) {
    this.answerType = answerType;
  }

  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  public static final Creator<Alarm> CREATOR = new Creator<Alarm>() {
    @Override
    public Alarm createFromParcel(Parcel in) {
      return new Alarm(in);
    }

    @Override
    public Alarm[] newArray(int size) {
      return new Alarm[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(id);
    dest.writeInt(hour);
    dest.writeInt(minute);
    dest.writeInt(days);
    dest.writeInt(state);
    dest.writeByte((byte) (isVibrate ? 1 : 0));
    dest.writeString(ringtone);
    dest.writeLong(time);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Alarm alarm = (Alarm) o;

    if (id != alarm.id) {
      return false;
    }
    if (hour != alarm.hour) {
      return false;
    }
    if (minute != alarm.minute) {
      return false;
    }
    if (days != alarm.days) {
      return false;
    }
    if (state != alarm.state) {
      return false;
    }
    if (isVibrate != alarm.isVibrate) {
      return false;
    }
    if (time != alarm.time) {
      return false;
    }
    if (answerType != alarm.answerType) {
      return false;
    }
    if (!ringtone.equals(alarm.ringtone)) {
      return false;
    }
    return Objects.equals(answer, alarm.answer);
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + hour;
    result = 31 * result + minute;
    result = 31 * result + days;
    result = 31 * result + state;
    result = 31 * result + (isVibrate ? 1 : 0);
    result = 31 * result + ringtone.hashCode();
    result = 31 * result + (int) (time ^ (time >>> 32));
    result = 31 * result + answerType;
    result = 31 * result + (answer != null ? answer.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Alarm{" +
        "id=" + id +
        ", hour=" + hour +
        ", minute=" + minute +
        ", days=" + days +
        ", state=" + state +
        ", isVibrate=" + isVibrate +
        ", ringtone='" + ringtone + '\'' +
        ", time=" + time +
        ", answerType=" + answerType +
        ", answer='" + answer + '\'' +
        '}';
  }
}