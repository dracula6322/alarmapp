package square.green.com.utils.api.databasealarm;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;
import task.job.example.utils.model.Alarm;

public interface DatabaseAlarmApi {

  @Nullable
  Alarm getAlarmById(long alarmId);

  boolean writeAlarm(@NonNull Alarm alarm);

  int deleteAlarm(long alarmId);

  @NonNull
  List getAlarms();
}
