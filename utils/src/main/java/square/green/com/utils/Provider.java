package square.green.com.utils;

import androidx.annotation.NonNull;

public interface Provider<T> {

  @NonNull
  T getValue();

}
