package square.green.com.utils.stub;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;
import square.green.com.utils.api.databasealarm.DatabaseAlarmApi;
import task.job.example.utils.model.Alarm;

public class FakeAlarmDatabaseWithValue implements DatabaseAlarmApi {

  private List<Alarm> alarms;

  public FakeAlarmDatabaseWithValue(List<Alarm> alarms) {
    this.alarms = alarms;
  }

  @Nullable
  @Override
  public Alarm getAlarmById(long alarmId) {

    for (Alarm alarm : alarms) {
      if (alarm.getId() == alarmId) {
        return alarm;
      }
    }
    return null;
  }

  @Override
  public boolean writeAlarm(@NonNull Alarm alarm) {
    return alarms.add(alarm);
  }

  @Override
  public int deleteAlarm(long alarmId) {
    for (int i = 0; i < alarms.size(); i++) {
      if (alarms.get(i).getId() == alarmId) {
        alarms.remove(i);
        return i;
      }
    }
    return -1;
  }

  @NonNull
  @Override
  public List<Alarm> getAlarms() {
    return alarms;
  }
}
