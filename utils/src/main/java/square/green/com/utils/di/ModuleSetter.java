package square.green.com.utils.di;

public interface ModuleSetter<T> {

  void set(T value);

}
