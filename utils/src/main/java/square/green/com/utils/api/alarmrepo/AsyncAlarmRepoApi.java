package square.green.com.utils.api.alarmrepo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import task.job.example.utils.model.Alarm;

public interface AsyncAlarmRepoApi extends AlarmRepoApi {


  void deleteAlarmById(long id, @NonNull ResponseRepo<Alarm> response);

  void getAlarmById(long id, @NonNull ResponseRepo<Alarm> response);

  void getAlarms(@NonNull ResponseRepo<List<Alarm>> response);

  void clearThread();

  class EmptyAsyncAlarmRepo implements AsyncAlarmRepoApi {

    @Override
    public void deleteAlarmById(long id, @NonNull ResponseRepo<Alarm> response) {

    }

    @Override
    public void getAlarmById(long id, @NonNull ResponseRepo<Alarm> response) {

    }

    @Override
    public void getAlarms(@NonNull ResponseRepo<List<Alarm>> response) {

    }

    @Override
    public void clearThread() {

    }

    @Nullable
    @Override
    public Alarm getAlarmById(long id) {
      return null;
    }

    @Nullable
    @Override
    public Alarm deleteAlarmById(long id) {
      return null;
    }

    @Override
    public boolean writeAlarm(@NonNull Alarm alarm) {
      return false;
    }

    @NonNull
    @Override
    public List<Alarm> getAlarms() {
      return Collections.emptyList();
    }
  }

}
