package square.green.com.utils.api.alarmrepo;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;
import task.job.example.utils.model.Alarm;

public interface AlarmRepoApi {

  @Nullable
  Alarm getAlarmById(long id);

  @Nullable
  Alarm deleteAlarmById(long id);

  boolean writeAlarm(@NonNull Alarm alarm);

  @NonNull
  List<Alarm> getAlarms();
}
