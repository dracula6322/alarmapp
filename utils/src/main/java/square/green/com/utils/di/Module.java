package square.green.com.utils.di;

public interface Module<T> {

  T get();

}
