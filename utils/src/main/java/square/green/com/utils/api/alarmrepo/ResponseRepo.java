package square.green.com.utils.api.alarmrepo;

import androidx.annotation.Nullable;

public interface ResponseRepo<T> {

  void getResult(@Nullable T result);
}
