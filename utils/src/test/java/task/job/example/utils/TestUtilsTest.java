package task.job.example.utils;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.junit.Before;
import org.junit.Test;

public class TestUtilsTest {

  private DateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm:ss Z");

  @Before
  public void setupTime() {
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
  }

  @Test
  public void delayWithNoTime() {

    int currentDays = Calendar.MONDAY;
    int currentHour = 18;
    int currentMinute = 18;

    int alarmDays = TimeUtils.dayFromCalendar(Calendar.MONDAY);
    int alarmHour = 18;
    int alarmMinute = 18;

    long nearDay = TimeUtils
        .getNearestDay(currentDays, currentHour, currentMinute,
            alarmDays, alarmHour, alarmMinute);

    System.out.println(dateFormat.format(nearDay));

    checkTwoCalendar(nearDay, 0, 0, 0);
  }

  @Test
  public void sameDaysButEarlierForMinute() {

    int currentDays = Calendar.MONDAY;
    int currentHour = 18;
    int currentMinute = 18;

    int alarmDays = TimeUtils.dayFromCalendar(Calendar.MONDAY);
    int alarmHour = 18;
    int alarmMinute = 17;

    long nearDay = TimeUtils
        .getNearestDay(currentDays, currentHour, currentMinute,
            alarmDays, alarmHour, alarmMinute);

    System.out.println(dateFormat.format(nearDay));

    checkTwoCalendar(nearDay, 6, 23, 59);
  }

  @Test
  public void sameDaysButAfterForMinute() {

    int currentDays = Calendar.MONDAY;
    int currentHour = 18;
    int currentMinute = 18;

    int alarmDays = TimeUtils.dayFromCalendar(Calendar.MONDAY);
    int alarmHour = 18;
    int alarmMinute = 19;

    long nearDay = TimeUtils
        .getNearestDay(currentDays, currentHour, currentMinute,
            alarmDays, alarmHour, alarmMinute);

    System.out.println(dateFormat.format(nearDay));

    checkTwoCalendar(nearDay, 0, 0, 1);
  }

  @Test
  public void nextDaysInSameTime() {

    int currentDays = Calendar.MONDAY;
    int currentHour = 18;
    int currentMinute = 18;

    int alarmDays = TimeUtils.dayFromCalendar(Calendar.TUESDAY);
    int alarmHour = 18;
    int alarmMinute = 18;

    long nearDay = TimeUtils
        .getNearestDay(currentDays, currentHour, currentMinute,
            alarmDays, alarmHour, alarmMinute);

    System.out.println(dateFormat.format(nearDay));

    checkTwoCalendar(nearDay, 1, 0, 0);
  }

  private void checkTwoCalendar(long nearDay, int day, int hour,
      int minute) {

    Calendar secondCalendar = new GregorianCalendar();
    secondCalendar.setTimeInMillis(nearDay);

//    System.out.println(dateFormat.format(firstCalendar.getTimeInMillis()));
//    System.out.println(dateFormat.format(secondCalendar.getTimeInMillis()));

    int differenceDays = (int) TimeUnit.MILLISECONDS.toDays(nearDay);

    assertEquals(differenceDays, day);

    int differenceHours = (int) (TimeUnit.MILLISECONDS.toHours(nearDay) - 24 * differenceDays);

    assertEquals(differenceHours, hour);

    int differenceMinutes = (int) (
        TimeUnit.MILLISECONDS.toMinutes(nearDay) - (differenceDays * 24 + differenceHours) * 60);

    assertEquals(differenceMinutes, minute);

    assertEquals(secondCalendar.get(Calendar.SECOND), 0);

    assertEquals(secondCalendar.get(Calendar.MILLISECOND), 0);

  }

}