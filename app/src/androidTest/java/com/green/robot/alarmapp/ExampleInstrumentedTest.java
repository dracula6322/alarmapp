package com.green.robot.alarmapp;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static task.job.example.utils.TimeUtils.SLIDER_TYPE;

import android.content.Context;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import square.green.com.alarmwork.AppNotificationManager;
import square.green.com.di.ApplicationComponent;
import square.green.com.di.ApplicationComponent.Builder;
import square.green.com.di.module.AlarmWorkerLiveModuleDI;
import square.green.com.di.module.AlarmWorkerModuleDI;
import square.green.com.di.module.AppModuleDI;
import square.green.com.di.module.DatabaseModuleDI;
import task.job.example.utils.model.Alarm;

@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

  Context context;

  @Before
  public void setup() {
    context = ApplicationProvider.getApplicationContext();
  }


  @Test
  public void checkActivityIsStarting() {

    ApplicationComponent applicationComponent = new Builder()
        .contextModule(new AppModuleDI(context))
        .databaseModule(new DatabaseModuleDI())
        //Injector.getInstance().addAlarmRepoModule(new AlarmRepoModuleDI());
        //.alarmRepoModule(new FakeAlarmRepoModuleDI())
        .alarmWorkerModule(new AlarmWorkerModuleDI())
        .alarmWorkerLiveModule(new AlarmWorkerLiveModuleDI())
        .build();

    AppNotificationManager.activity = LaunchActivity.class;

    final long alarmId = 1001;
    Alarm alarmForService = new Alarm();
    alarmForService.setId(alarmId);
    alarmForService.setVibrate(true);
    alarmForService.setRingtone("asdasdadas");

    boolean result = applicationComponent.provideAlarmWorkLiveApi()
        .startAlarmService(context, alarmForService, SLIDER_TYPE);

    try {
      Thread.sleep(1500);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    assertThat(result, is(true));
    assertEquals(
        (long) applicationComponent.provideAlarmWorkLiveApi().getLiveDataWorking().getValue(),
        alarmId);

    applicationComponent.provideAlarmWorker().stopService(context);

    try {
      Thread.sleep(1500);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    assertEquals(
        (long) applicationComponent.provideAlarmWorkLiveApi().getLiveDataWorking().getValue(),
        -1);

  }

}
