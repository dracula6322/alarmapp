package com.green.robot.alarmapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.collection.LongSparseArray;
import androidx.lifecycle.Observer;
import java.util.Calendar;
import java.util.TimeZone;
import square.green.com.utils.api.alarmrepo.AlarmRepoApi;
import task.job.example.utils.FileLog;
import task.job.example.utils.TimeUtils;
import task.job.example.utils.api.AlarmWorkApi;
import task.job.example.utils.model.Alarm;
import task.job.example.utils.response.CreateAlarmReport;


public class SetTimeBroadcastReceiver extends BroadcastReceiver implements
    Observer<LongSparseArray<Alarm>> {

    public AlarmRepoApi repoAlarm;
    public AlarmWorkApi appAlarmManager;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent == null)
            return;

        if (intent.getAction() == null)
            return;

        if (!(intent.getAction().equalsIgnoreCase(Intent.ACTION_TIME_CHANGED) ||
                intent.getAction().equalsIgnoreCase(Intent.ACTION_TIMEZONE_CHANGED)))
            return;

        FileLog.getInstance().v("SetTimeBroadcastReceiver " + intent.getAction());

///        repoAlarm.observe(this);

    }


    @Override
    public void onChanged(@Nullable LongSparseArray<Alarm> alarmWithTasks) {

        if (alarmWithTasks == null)
            return;

        for (int i = 0; i < alarmWithTasks.size(); i++) {

          Alarm alarm = alarmWithTasks.valueAt(i);
            if (alarm.getState() == Alarm.ALARM_OFF)
                continue;

            //appAlarmManager.cancelAlarm(alarm);

            alarm.setState(Alarm.ALARM_OFF);
//            repoAlarm.updateAlarm(alarm);

          int alarmTaskType = alarm.getAnswerType();

          Calendar launchCalendar = Calendar.getInstance();
          launchCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
          launchCalendar.setTimeInMillis(System.currentTimeMillis());

          int currentDays = launchCalendar.get(Calendar.DAY_OF_WEEK);
          int currentHour = launchCalendar.get(Calendar.HOUR_OF_DAY);
          int currentMinute = launchCalendar.get(Calendar.MINUTE);

          CreateAlarmReport report = appAlarmManager
              .setAlarm(alarmTaskType, currentDays, currentHour, currentMinute, alarm.getDays(),
                  alarm.getHour(), alarm.getMinute(), alarm.getId(),
                  launchCalendar.getTime().getTime());

            long time = report.getTime();

            alarm.setState(Alarm.ALARM_ON);
            alarm.setTime(time);
          repoAlarm.writeAlarm(alarm);

            FileLog.getInstance().v("SetTimeBroadcastReceiver alarm recount " + alarm.getId() + " " + time + " " + TimeUtils.stringTimeToAlarm(time, false));
        }

      //repoAlarm.stopObserve(this);

    }
}
