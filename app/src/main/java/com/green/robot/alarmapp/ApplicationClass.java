package com.green.robot.alarmapp;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.util.Log;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import square.green.com.di.ApplicationComponent;
import square.green.com.di.ApplicationComponent.Builder;
import square.green.com.di.ApplicationComponentImpl;
import square.green.com.di.module.AlarmWorkerLiveModuleDI;
import square.green.com.di.module.AlarmWorkerModuleDI;
import square.green.com.di.module.AppModuleDI;
import square.green.com.di.module.DatabaseModuleDI;
import square.green.com.di.module.alarmRepo.AlarmRepoModuleDI;

public class ApplicationClass extends Application {

  @SuppressLint("StaticField")
  static public volatile Context context;
  static public RefWatcher watcher;

  @Override
  public void onCreate() {
    super.onCreate();

    context = getApplicationContext();
    Log.v("TAG", "We are in applicationClass");

    ApplicationComponent applicationComponent = new Builder()
        .contextModule(new AppModuleDI(context))
        .databaseModule(new DatabaseModuleDI())
        .alarmRepoModule(new AlarmRepoModuleDI())
        .alarmWorkerModule(new AlarmWorkerModuleDI())
        .alarmWorkerLiveModule(new AlarmWorkerLiveModuleDI())
        .build();

    ApplicationComponentImpl.getInstance().
        setApplicationComponent(applicationComponent);

    if (BuildConfig.DEBUG) {
      if (LeakCanary.isInAnalyzerProcess(this)) {
        return;
      }
      watcher = LeakCanary.install(this);
    }
  }


}


