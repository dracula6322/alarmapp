package com.green.robot.alarmapp;

import static task.job.example.utils.api.AlarmWorkApi.FROM_ALARM_B;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.navigation.NavigationView;
import square.green.com.fragmentmanager.AppFragmentManager;
import task.job.example.utils.FileLog;

public class LaunchActivity extends AppCompatActivity {

  public DrawerLayout linearLayout;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    FileLog.getInstance().v("Start Activity "
        + getIntent().getBooleanExtra(FROM_ALARM_B, false)
        + (" savedInstanceState is null" + (savedInstanceState == null))
    );

    initFragmentManagerLifeCycleDebug();

    setTheme(R.style.AppTheme);

    initView(this);

    AppFragmentManager.getInstance().onCreate(getSupportFragmentManager(), getIntent());
  }

  private void initFragmentManagerLifeCycleDebug() {
    getSupportFragmentManager().registerFragmentLifecycleCallbacks(lifecycleCallback, true);
  }

  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);

    if (intent == null) {
      return;
    }

    FileLog.getInstance().v("onNewIntent " + intent.toString());

    if ("android.intent.action.MAIN".equals(intent.getAction())) {
      return;
    }

    AppFragmentManager.getInstance().onNewIntent(getSupportFragmentManager(), intent);
  }

  private void initView(Activity activity) {

    linearLayout = new DrawerLayout(this);
    linearLayout.setLayoutParams(new DrawerLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
        ViewGroup.LayoutParams.MATCH_PARENT));
    linearLayout.setFitsSystemWindows(true);

    FrameLayout frameLayout = new FrameLayout(this);
    frameLayout.setId(AppFragmentManager.idFragment);
    frameLayout.setLayoutParams(
        new DrawerLayout.LayoutParams(DrawerLayout.LayoutParams.MATCH_PARENT,
            DrawerLayout.LayoutParams.MATCH_PARENT));
    linearLayout.addView(frameLayout);

    NavigationView navigationView = new NavigationView(this);
    navigationView.addHeaderView(getLayoutInflater().inflate(R.layout.nav_header_main, null));
    navigationView.inflateMenu(R.menu.activity_main_drawer);

    DrawerLayout.LayoutParams navLayout = new DrawerLayout.LayoutParams(
        DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT,
        GravityCompat.START);
    linearLayout.addView(navigationView, navLayout);

    activity.setContentView(linearLayout);
  }

  @Override
  public void onBackPressed() {
    //AppFragmentManager.getInstance().onBackPressedNewVersion(this);
  }

  @Override
  protected void onStart() {
    FileLog.getInstance().v("LC", "LaunchActivity onStart");
    super.onStart();
  }

  @Override
  protected void onResume() {
    FileLog.getInstance().v("LC", "LaunchActivity onResume");
    super.onResume();
  }

  @Override
  protected void onStop() {
    FileLog.getInstance().v("LC", "LaunchActivity onStop");
    getSupportFragmentManager().unregisterFragmentLifecycleCallbacks(lifecycleCallback);
    super.onStop();
  }

  private String getClassName(Class mClass) {
    return mClass.getName().substring(mClass.getName().lastIndexOf('.') + 1);
  }


  FragmentManager.FragmentLifecycleCallbacks lifecycleCallback = new FragmentManager.FragmentLifecycleCallbacks() {
    @Override
    public void onFragmentCreated(@NonNull FragmentManager fm, @NonNull Fragment f,
        Bundle savedInstanceState) {
      super.onFragmentCreated(fm, f, savedInstanceState);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentCreated " + (f.getTag() != null ? f.getTag()
              : "") + (savedInstanceState != null ? " state true" : ""));
    }

    @Override
    public void onFragmentPreAttached(@NonNull FragmentManager fm, @NonNull Fragment f,
        @NonNull Context context) {
      super.onFragmentPreAttached(fm, f, context);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentPreAttached " + (f.getTag() != null ? f.getTag()
              : ""));
    }

    @Override
    public void onFragmentAttached(@NonNull FragmentManager fm, @NonNull Fragment f,
        @NonNull Context context) {
      super.onFragmentAttached(fm, f, context);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentAttached " + (f.getTag() != null ? f.getTag()
              : ""));
    }

    @Override
    public void onFragmentPreCreated(@NonNull FragmentManager fm, @NonNull Fragment f,
        Bundle savedInstanceState) {
      super.onFragmentPreCreated(fm, f, savedInstanceState);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentPreCreated " + (f.getTag() != null ? f.getTag()
              : "") + (savedInstanceState != null ? " state true" : ""));
    }

    @Override
    public void onFragmentActivityCreated(@NonNull FragmentManager fm, @NonNull Fragment f,
        Bundle savedInstanceState) {
      super.onFragmentActivityCreated(fm, f, savedInstanceState);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentActivityCreated " + (f.getTag() != null ? f
              .getTag() : "") + (savedInstanceState != null ? " state true" : ""));

    }

    @Override
    public void onFragmentViewCreated(@NonNull FragmentManager fm, @NonNull Fragment f,
        @NonNull View v, Bundle savedInstanceState) {
      super.onFragmentViewCreated(fm, f, v, savedInstanceState);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentViewCreated " + (f.getTag() != null ? f.getTag()
              : "") + (savedInstanceState != null ? " state true" : ""));
    }

    @Override
    public void onFragmentStarted(@NonNull FragmentManager fm, @NonNull Fragment f) {
      super.onFragmentStarted(fm, f);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentStarted " + (f.getTag() != null ? f.getTag()
              : ""));
    }

    @Override
    public void onFragmentResumed(@NonNull FragmentManager fm, @NonNull Fragment f) {
      super.onFragmentResumed(fm, f);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentResumed " + (f.getTag() != null ? f.getTag()
              : ""));
    }

    @Override
    public void onFragmentPaused(@NonNull FragmentManager fm, @NonNull Fragment f) {
      super.onFragmentPaused(fm, f);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentPaused " + (f.getTag() != null ? f.getTag()
              : ""));
    }

    @Override
    public void onFragmentStopped(@NonNull FragmentManager fm, @NonNull Fragment f) {
      super.onFragmentStopped(fm, f);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentStopped " + (f.getTag() != null ? f.getTag()
              : ""));
    }

    @Override
    public void onFragmentSaveInstanceState(@NonNull FragmentManager fm, @NonNull Fragment f,
        @NonNull Bundle outState) {
      super.onFragmentSaveInstanceState(fm, f, outState);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentSaveInstanceState " + (f.getTag() != null ? f
              .getTag() : ""));
    }

    @Override
    public void onFragmentViewDestroyed(@NonNull FragmentManager fm, @NonNull Fragment f) {
      super.onFragmentViewDestroyed(fm, f);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentViewDestroyed " + (f.getTag() != null ? f
              .getTag() : ""));
    }

    @Override
    public void onFragmentDestroyed(@NonNull FragmentManager fm, @NonNull Fragment f) {
      super.onFragmentDestroyed(fm, f);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentDestroyed " + (f.getTag() != null ? f.getTag()
              : ""));
    }

    @Override
    public void onFragmentDetached(@NonNull FragmentManager fm, @NonNull Fragment f) {
      super.onFragmentDetached(fm, f);
      FileLog.getInstance().v("LC",
          getClassName(f.getClass()) + " onFragmentDetached " + (f.getTag() != null ? f.getTag()
              : ""));
    }
  };


}

